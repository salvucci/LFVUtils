#include <LFVUtils/MCSampleInfo.h>

using namespace LFVUtils;

double MCSampleInfo :: GetNumberEvents_13TeV(int channelNumber){
  
  m_numEvt = -1;

  /* Zee and DYee */
  if( channelNumber == 361106 ){       m_name = "Powheg+Pythia Zee";                  m_numEvt = 1.502776e+11;      }
  else if( channelNumber == 301540 ){  m_name = "Pythia8 DYee 70M120";                m_numEvt = 2.48400000e+05;    }
  else if( channelNumber == 301541 ){  m_name = "Pythia8 DYee 120M120";               m_numEvt = 2.49200000e+05;    }
  else if( channelNumber == 301542 ){  m_name = "Pythia8 DYee 180M250";               m_numEvt = 2.49200000e+05;    }
  else if( channelNumber == 301543 ){  m_name = "Pythia8 DYee 250M400";               m_numEvt = 1.49800000e+05;    }
  else if( channelNumber == 301544 ){  m_name = "Pythia8 DYee 400M600";               m_numEvt = 9.94000000e+04;    }
  else if( channelNumber == 301545 ){  m_name = "Pythia8 DYee 600M800";               m_numEvt = 4.98000000e+04;    }
  else if( channelNumber == 301546 ){  m_name = "Pythia8 DYee 800M1000";              m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301547 ){  m_name = "Pythia8 DYee 1000M1250";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301548 ){  m_name = "Pythia8 DYee 1250M1500";             m_numEvt = 4.98000000e+04;    }
  else if( channelNumber == 301549 ){  m_name = "Pythia8 DYee 1500M1750";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301550 ){  m_name = "Pythia8 DYee 1750M2000";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301551 ){  m_name = "Pythia8 DYee 2000M2250";             m_numEvt = 4.98000000e+04;    }
  else if( channelNumber == 301552 ){  m_name = "Pythia8 DYee 2250M2500";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301553 ){  m_name = "Pythia8 DYee 2500M2750";             m_numEvt = 4.96000000e+04;    }
  else if( channelNumber == 301554 ){  m_name = "Pythia8 DYee 2750M3000";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301555 ){  m_name = "Pythia8 DYee 3000M3500";             m_numEvt = 4.96000000e+04;    }
  else if( channelNumber == 301556 ){  m_name = "Pythia8 DYee 2500M4000";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301557 ){  m_name = "Pythia8 DYee 4000M4500";             m_numEvt = 4.98000000e+04;    }
  else if( channelNumber == 301558 ){  m_name = "Pythia8 DYee 4500M5000";             m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301559 ){  m_name = "Pythia8 DYee 5000M";                 m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301000 ){  m_name = "PowhegPythia8 DYee 120M180";         m_numEvt = 8.70426e+06;       }
  else if( channelNumber == 301001 ){  m_name = "PowhegPythia8 DYee 180M250";         m_numEvt = 729132;            }
  else if( channelNumber == 301002 ){  m_name = "PowhegPythia8 DYee 250M400";         m_numEvt = 161638;            }
  else if( channelNumber == 301003 ){  m_name = "PowhegPythia8 DYee 400M600";         m_numEvt = 19549.6;           }
  else if( channelNumber == 301004 ){  m_name = "PowhegPythia8 DYee 600M800";         m_numEvt = 5423.16;           }
  else if( channelNumber == 301005 ){  m_name = "PowhegPythia8 DYee 800M1000";        m_numEvt = 530.326;           }
  else if( channelNumber == 301006 ){  m_name = "PowhegPythia8 DYee 1000M1250";       m_numEvt = 212.908;           }
  else if( channelNumber == 301007 ){  m_name = "PowhegPythia8 DYee 1250M1500";       m_numEvt = 71.0944;           }
  else if( channelNumber == 301008 ){  m_name = "PowhegPythia8 DYee 1500M1750";       m_numEvt = 27.1515;           }
  else if( channelNumber == 301009 ){  m_name = "PowhegPythia8 DYee 1750M2000";       m_numEvt = 12.2771;           }
  else if( channelNumber == 301010 ){  m_name = "PowhegPythia8 DYee 2000M2250";       m_numEvt = 5.1926;            }
  else if( channelNumber == 301011 ){  m_name = "PowhegPythia8 DYee 2250M2500";       m_numEvt = 2.46996;           }
  else if( channelNumber == 301012 ){  m_name = "PowhegPythia8 DYee 2500M2750";       m_numEvt = 1.22263;           }
  else if( channelNumber == 301013 ){  m_name = "PowhegPythia8 DYee 2750M3000";       m_numEvt = 0.596956;          }
  else if( channelNumber == 301014 ){  m_name = "PowhegPythia8 DYee 3000M3500";       m_numEvt = 0.487483;          }
  else if( channelNumber == 301015 ){  m_name = "PowhegPythia8 DYee 3500M4000";       m_numEvt = 0.139666;          }
  else if( channelNumber == 301016 ){  m_name = "PowhegPythia8 DYee 4000M4500";       m_numEvt = 0.0430877;         }
  else if( channelNumber == 301017 ){  m_name = "PowhegPythia8 DYee 4500M5000";       m_numEvt = 0.0261054;         }
  else if( channelNumber == 301018 ){  m_name = "PowhegPythia8 DYee 5000M";           m_numEvt = 0.00607124;        }

  /* Zmumu and DYmumu */
  else if( channelNumber == 361107 ){  m_name = "Powheg+Pythia Zmumu";                m_numEvt = 1.473346e+11;      }
  else if( channelNumber == 301560 ){  m_name = "Pythia8 DYmumu 70M120";              m_numEvt = 2.49200000e+05;    }
  else if( channelNumber == 301561 ){  m_name = "Pythia8 DYmumu 120M180";             m_numEvt = 2.49400000e+05;    }
  else if( channelNumber == 301562 ){  m_name = "Pythia8 DYmumu 180M250";             m_numEvt = 2.49000000e+05;    }
  else if( channelNumber == 301563 ){  m_name = "Pythia8 DYmumu 350M400";             m_numEvt = 1.50000000e+05;    }
  else if( channelNumber == 301564 ){  m_name = "Pythia8 DYmumu 400M600";             m_numEvt = 9.96000000e+04;    }
  else if( channelNumber == 301565 ){  m_name = "Pythia8 DYmumu 600M800";             m_numEvt = 4.96000000e+04;    }
  else if( channelNumber == 301566 ){  m_name = "Pythia8 DYmumu 800M1000";            m_numEvt = 4.80000000e+04;    }
  else if( channelNumber == 301567 ){  m_name = "Pythia8 DYmumu 1000M1250";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301568 ){  m_name = "Pythia8 DYmumu 1250M1500";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301569 ){  m_name = "Pythia8 DYmumu 1500M1750";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301570 ){  m_name = "Pythia8 DYmumu 1750M2000";           m_numEvt = 4.96000000e+04;    }
  else if( channelNumber == 301571 ){  m_name = "Pythia8 DYmumu 2000M2250";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301572 ){  m_name = "Pythia8 DYmumu 2250M2500";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301573 ){  m_name = "Pythia8 DYmumu 2500M2750";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301574 ){  m_name = "Pythia8 DYmumu 2750M3000";           m_numEvt = 4.98000000e+04;    }
  else if( channelNumber == 301575 ){  m_name = "Pythia8 DYmumu 3000M3500";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301576 ){  m_name = "Pythia8 DYmumu 3500M4000";           m_numEvt = 4.98000000e+04;    }
  else if( channelNumber == 301577 ){  m_name = "Pythia8 DYmumu 4000M4500";           m_numEvt = 4.92000000e+04;    }
  else if( channelNumber == 301578 ){  m_name = "Pythia8 DYmumu 4500M5000";           m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301579 ){  m_name = "Pythia8 DYmumu 5000M";               m_numEvt = 5.00000000e+04;    }
  else if( channelNumber == 301020 ){  m_name = "PowhegPythia8 DYmumu 120M180";       m_numEvt = 8.7148e+06;        }
  else if( channelNumber == 301021 ){  m_name = "PowhegPythia8 DYmumu 180M250";       m_numEvt = 729133;            }
  else if( channelNumber == 301022 ){  m_name = "PowhegPythia8 DYmumu 250M400";       m_numEvt = 161419;            }
  else if( channelNumber == 301023 ){  m_name = "PowhegPythia8 DYmumu 400M600";       m_numEvt = 19354.1;           }
  else if( channelNumber == 301024 ){  m_name = "PowhegPythia8 DYmumu 600M800";       m_numEvt = 2266.35;           }
  else if( channelNumber == 301025 ){  m_name = "PowhegPythia8 DYmumu 800M1000";      m_numEvt = 530.326;           }
  else if( channelNumber == 301026 ){  m_name = "PowhegPythia8 DYmumu 1000M1250";     m_numEvt = 212.908;           }
  else if( channelNumber == 301027 ){  m_name = "PowhegPythia8 DYmumu 1250M1500";     m_numEvt = 70.8099;           }
  else if( channelNumber == 301028 ){  m_name = "PowhegPythia8 DYmumu 1500M1750";     m_numEvt = 27.0424;           }
  else if( channelNumber == 301029 ){  m_name = "PowhegPythia8 DYmumu 1750M2000";     m_numEvt = 11.4952;           }
  else if( channelNumber == 301030 ){  m_name = "PowhegPythia8 DYmumu 2000M2250";     m_numEvt = 5.1926;            }
  else if( channelNumber == 301031 ){  m_name = "PowhegPythia8 DYmumu 2250M2500";     m_numEvt = 2.46008;           }
  else if( channelNumber == 301032 ){  m_name = "PowhegPythia8 DYmumu 2500M2750";     m_numEvt = 1.21775;           }
  else if( channelNumber == 301033 ){  m_name = "PowhegPythia8 DYmumu 2750M3000";     m_numEvt = 0.624437;          }
  else if( channelNumber == 301034 ){  m_name = "PowhegPythia8 DYmumu 3000M3500";     m_numEvt = 0.501532;          }
  else if( channelNumber == 301035 ){  m_name = "PowhegPythia8 DYmumu 3500M4000";     m_numEvt = 0.146709;          }
  else if( channelNumber == 301036 ){  m_name = "PowhegPythia8 DYmumu 4000M4500";     m_numEvt = 0.0439837;         }
  else if( channelNumber == 301037 ){  m_name = "PowhegPythia8 DYmumu 4500M5000";     m_numEvt = 0.014035;          }
  else if( channelNumber == 301038 ){  m_name = "PowhegPythia8 DYmumu 5000M";         m_numEvt = 0.0063242;         }
  
  /* Ztautau and DYtautau */
  else if( channelNumber == 361108 ){  m_name = "Powheg+Pythia Ztautau";              m_numEvt = 5.617166e+10;      }
  else if( channelNumber == 303437 ){  m_name = "Pythia8 DYtautau 120M180";           m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303438 ){  m_name = "Pythia8 DYtautau 180M250";           m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303439 ){  m_name = "Pythia8 DYtautau 250M400";           m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303440 ){  m_name = "Pythia8 DYtautau 400M600";           m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303441 ){  m_name = "Pythia8 DYtautau 600M800";           m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303442 ){  m_name = "Pythia8 DYtautau 800M1000";          m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303443 ){  m_name = "Pythia8 DYtautau 1000M1250";         m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303444 ){  m_name = "Pythia8 DYtautau 1250M1500";         m_numEvt = 2.9940000000e+05;  }
  else if( channelNumber == 303445 ){  m_name = "Pythia8 DYtautau 1500M1750";         m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303446 ){  m_name = "Pythia8 DYtautau 1750M2000";         m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303447 ){  m_name = "Pythia8 DYtautau 2000M2250";         m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303448 ){  m_name = "Pythia8 DYtautau 2250M2500";         m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303449 ){  m_name = "Pythia8 DYtautau 2500M2750";         m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303450 ){  m_name = "Pythia8 DYtautau 2750M3000";         m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303451 ){  m_name = "Pythia8 DYtautau 3000M3500";         m_numEvt = 2.9980000000e+05;  }
  else if( channelNumber == 303452 ){  m_name = "Pythia8 DYtautau 3500M4000";         m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303453 ){  m_name = "Pythia8 DYtautau 4000M4500";         m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303454 ){  m_name = "Pythia8 DYtautau 4500M5000";         m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 303455 ){  m_name = "Pythia8 DYtautau 5000M";             m_numEvt = 3.0000000000e+05;  }
  else if( channelNumber == 301040 ){  m_name = "PowhegPythia8 DYtautau 120M180";     m_numEvt = 2.61841e+06;       }
  else if( channelNumber == 301041 ){  m_name = "PowhegPythia8 DYtautau 180M250";     m_numEvt = 437540;            }
  else if( channelNumber == 301042 ){  m_name = "PowhegPythia8 DYtautau 250M400";     m_numEvt = 161854;            }
  else if( channelNumber == 301043 ){  m_name = "PowhegPythia8 DYtautau 400M600";     m_numEvt = 28972.7;           }
  else if( channelNumber == 301044 ){  m_name = "PowhegPythia8 DYtautau 600M800";     m_numEvt = 5610.17;           }
  else if( channelNumber == 301045 ){  m_name = "PowhegPythia8 DYtautau 800M1000";    m_numEvt = 1584.69;           }
  else if( channelNumber == 301046 ){  m_name = "PowhegPythia8 DYtautau 1000M1250";   m_numEvt = 630.235;           }
  else if( channelNumber == 301047 ){  m_name = "PowhegPythia8 DYtautau 1250M1500";   m_numEvt = 213.009;           }
  else if( channelNumber == 301048 ){  m_name = "PowhegPythia8 DYtautau 1500M1750";   m_numEvt = 27.2606;           }
  else if( channelNumber == 301049 ){  m_name = "PowhegPythia8 DYtautau 1750M2000";   m_numEvt = 11.0812;           }
  else if( channelNumber == 301050 ){  m_name = "PowhegPythia8 DYtautau 2000M2250";   m_numEvt = 10.2821;           }
  else if( channelNumber == 301051 ){  m_name = "PowhegPythia8 DYtautau 2250M2500";   m_numEvt = 2.46996;           }
  else if( channelNumber == 301052 ){  m_name = "PowhegPythia8 DYtautau 2500M2750";   m_numEvt = 1.22263;           }
  else if( channelNumber == 301053 ){  m_name = "PowhegPythia8 DYtautau 2750M3000";   m_numEvt = 0.624437;          }
  else if( channelNumber == 301054 ){  m_name = "PowhegPythia8 DYtautau 3000M3500";   m_numEvt = 0.497518;          }
  else if( channelNumber == 301055 ){  m_name = "PowhegPythia8 DYtautau 3500M4000";   m_numEvt = 0.143765;          }
  else if( channelNumber == 301056 ){  m_name = "PowhegPythia8 DYtautau 4000M4500";   m_numEvt = 0.0269274;         }
  else if( channelNumber == 301057 ){  m_name = "PowhegPythia8 DYtautau 4500M5000";   m_numEvt = 0.014035;          }
  else if( channelNumber == 301058 ){  m_name = "PowhegPythia8 DYtautau 5000M";       m_numEvt = 0.0063242;         }
  

  /* ttbar and SingleTop */
  else if( channelNumber == 410000 ){  m_name = "Powheg+Pythia ttbar_hdamp172p5_nonallhad";    m_numEvt = 4.93866e+07;     }
  else if( channelNumber == 410001 ){  m_name = "Powheg+Pythia ttbar_hdamp345_down_nonallhad"; m_numEvt = 1.79610000e+07;  }
  else if( channelNumber == 410002 ){  m_name = "Powheg+Pythia ttbar_hdamp345_up_nonallhad";   m_numEvt = 1.99000000e+07;  }
  else if( channelNumber == 410003 ){  m_name = "aMcAtNlo+Herwigpp ttbar_nonallhad";           m_numEvt = 2.89884800e+06;  }
  else if( channelNumber == 410007 ){  m_name = "Powheg+Pythia ttbar_hdamp172p5_allhad";       m_numEvt = 6.95735836e+00;  }
  else if( channelNumber == 410009 ){  m_name = "Powheg+Pythia ttbar_hdamp172p5_dilepton";     m_numEvt = 1.78449000e+07;  }							    
  else if( channelNumber == 410021 ){  m_name = "Sherpa ttbar DiLepton";                       m_numEvt = 1.00000000;      }
  else if( channelNumber == 410022 ){  m_name = "Sherpa ttbar SingleLeptonP";                  m_numEvt = 1.00000000;      }
  else if( channelNumber == 410023 ){  m_name = "Sherpa ttbar SingleLeptonM";                  m_numEvt = 1.00000000;      }
  else if( channelNumber == 410470 ){  m_name = "Powheg+Pythia ttbar_hdamp258p75_nonallhad";   m_numEvt = 1.00000000;      }
  else if( channelNumber == 306600 ){  m_name = "Sherpa 221 ttbar_ee_0M150";                   m_numEvt = 410547;          }
  else if( channelNumber == 306601 ){  m_name = "Sherpa 221 ttbar_ee_150M250";                 m_numEvt = 203164;          }
  else if( channelNumber == 306602 ){  m_name = "Sherpa 221 ttbar_ee_250M500";                 m_numEvt = 201044;          }
  else if( channelNumber == 306603 ){  m_name = "Sherpa 221 ttbar_ee_500M1000";                m_numEvt = 203092;          }
  else if( channelNumber == 306604 ){  m_name = "Sherpa 221 ttbar_ee_1000M2000";               m_numEvt = 100007;          }
  else if( channelNumber == 306605 ){  m_name = "Sherpa 221 ttbar_ee_2000M3000";               m_numEvt = 95822.1;         }
  else if( channelNumber == 306606 ){  m_name = "Sherpa 221 ttbar_ee_3000M4000";               m_numEvt = 83101.1;         }
  else if( channelNumber == 306607 ){  m_name = "Sherpa 221 ttbar_ee_4000M5000";               m_numEvt = 64273;           }
  else if( channelNumber == 306608 ){  m_name = "Sherpa 221 ttbar_ee_5000M";                   m_numEvt = 52456.7;         }
  else if( channelNumber == 306609 ){  m_name = "Sherpa 221 ttbar_mumu_0M150";                 m_numEvt = 401242;          }
  else if( channelNumber == 306610 ){  m_name = "Sherpa 221 ttbar_mumu_150M250";               m_numEvt = 202814;          }
  else if( channelNumber == 306611 ){  m_name = "Sherpa 221 ttbar_mumu_250M500";               m_numEvt = 201280;          }
  else if( channelNumber == 306612 ){  m_name = "Sherpa 221 ttbar_mumu_500M1000";              m_numEvt = 196545;          }
  else if( channelNumber == 306613 ){  m_name = "Sherpa 221 ttbar_mumu_1000M2000";             m_numEvt = 101374;          }
  else if( channelNumber == 306614 ){  m_name = "Sherpa 221 ttbar_mumu_2000M3000";             m_numEvt = 94613.7;         }
  else if( channelNumber == 306615 ){  m_name = "Sherpa 221 ttbar_mumu_3000M4000";             m_numEvt = 82407.4;         }
  else if( channelNumber == 306616 ){  m_name = "Sherpa 221 ttbar_mumu_4000M5000";             m_numEvt = 64495;           }
  else if( channelNumber == 306617 ){  m_name = "Sherpa 221 ttbar_mumu_5000M";                 m_numEvt = 51126.4;         }
  else if( channelNumber == 307479 ){  m_name = "Sherpa 221 ttbar_emu_0M150";                  m_numEvt = 299444;          }
  else if( channelNumber == 307480 ){  m_name = "Sherpa 221 ttbar_emu_150M500";                m_numEvt = 410228;          }
  else if( channelNumber == 307481 ){  m_name = "Sherpa 221 ttbar_emu_500M1000";               m_numEvt = 199682;          }
  else if( channelNumber == 307482 ){  m_name = "Sherpa 221 ttbar_emu_1000M2000";              m_numEvt = 199958;          }
  else if( channelNumber == 307483 ){  m_name = "Sherpa 221 ttbar_emu_2000M3000";              m_numEvt = 143674;          }
  else if( channelNumber == 307484 ){  m_name = "Sherpa 221 ttbar_emu_M3000";                  m_numEvt = 120077;          }
  else if( channelNumber == 307485 ){  m_name = "Sherpa 221 ttbar_etau_0M150";                 m_numEvt = 304213;          }
  else if( channelNumber == 307486 ){  m_name = "Sherpa 221 ttbar_etau_150M500";               m_numEvt = 410117;          }
  else if( channelNumber == 307487 ){  m_name = "Sherpa 221 ttbar_etau_500M1000";              m_numEvt = 204381;          }
  else if( channelNumber == 307488 ){  m_name = "Sherpa 221 ttbar_etau_1000M2000";             m_numEvt = 198453;          }
  else if( channelNumber == 307489 ){  m_name = "Sherpa 221 ttbar_etau_2000M3000";             m_numEvt = 141442;          }
  else if( channelNumber == 307490 ){  m_name = "Sherpa 221 ttbar_etau_M3000";                 m_numEvt = 126324;          }
  else if( channelNumber == 307491 ){  m_name = "Sherpa 221 ttbar_mutau_0M150";                m_numEvt = 302410;          }
  else if( channelNumber == 307492 ){  m_name = "Sherpa 221 ttbar_mutau_150M500";              m_numEvt = 393071;          }
  else if( channelNumber == 307493 ){  m_name = "Sherpa 221 ttbar_mutau_500M1000";             m_numEvt = 202748;          }
  else if( channelNumber == 307494 ){  m_name = "Sherpa 221 ttbar_mutau_1000M2000";            m_numEvt = 202133;          }
  else if( channelNumber == 307495 ){  m_name = "Sherpa 221 ttbar_mutau_2000M3000";            m_numEvt = 142892;          }
  else if( channelNumber == 307496 ){  m_name = "Sherpa 221 ttbar_mutau_M3000";                m_numEvt = 123326;          }
  else if( channelNumber == 307497 ){  m_name = "Sherpa 221 ttbar_tautau_0M150";               m_numEvt = 296539;          }
  else if( channelNumber == 307498 ){  m_name = "Sherpa 221 ttbar_tautau_150M500";             m_numEvt = 406372;          }
  else if( channelNumber == 307499 ){  m_name = "Sherpa 221 ttbar_tautau_500M1000";            m_numEvt = 199511;          }
  else if( channelNumber == 307500 ){  m_name = "Sherpa 221 ttbar_tautau_1000M2000";           m_numEvt = 201843;          }
  else if( channelNumber == 307501 ){  m_name = "Sherpa 221 ttbar_tautau_2000M3000";           m_numEvt = 147327;          }
  else if( channelNumber == 307502 ){  m_name = "Sherpa 221 ttbar_tautau_M3000";               m_numEvt = 124780;          }

  /* ttbar+X */
  else if( channelNumber == 410142 ){  m_name = "Sherpa 22 ttll_mll5";                         m_numEvt = 1.0; }
  else if( channelNumber == 410143 ){  m_name = "Sherpa 22 ttZnnqq";                           m_numEvt = 1.0; }
  else if( channelNumber == 410144 ){  m_name = "Sherpa 22 ttW";                               m_numEvt = 1.0; }

  /* SingleTop*/
  else if( channelNumber == 410013 ){  m_name = "Powheg+Pythia Wt_inclusive_top";              m_numEvt = 4.9858e+06;      }
  else if( channelNumber == 410014 ){  m_name = "Powheg+Pythia Wt_inclusive_antitop";          m_numEvt = 4.9856e+06;      }

  
  /* W+jets */
  else if( channelNumber == 361100 ){  m_name = "Powheg+Pythia8 Wplusenu";         m_numEvt = 3.38035867648000000e+11;  }					        
  else if( channelNumber == 361101 ){  m_name = "Powheg+Pythia8 Wplusmunu";        m_numEvt = 2.26090958848000000e+11;  }					        
  else if( channelNumber == 361102 ){  m_name = "Powheg+Pythia8 Wplustaunu";       m_numEvt = 3.38246467584000000e+11;  }					        
  else if( channelNumber == 361103 ){  m_name = "Powheg+Pythia8 Wminusenu";        m_numEvt = 1.65010915328000000e+11;  }					        
  else if( channelNumber == 361104 ){  m_name = "Powheg+Pythia8 Wminusmunu";       m_numEvt = 1.65405261824000000e+11;  }					        
  else if( channelNumber == 361105 ){  m_name = "Powheg+Pythia8 Wminustaunu";      m_numEvt = 1.65278629888000000e+11;  }					        
					        
  
  /* W+jets  - HT, PTV slice */
  else if( channelNumber == 364156 ){  m_name = "Sherpa Wmunu_MAXHTPTV0_70_CVetoBVeto";       m_numEvt = 1.66193e+07;  }
  else if( channelNumber == 364157 ){  m_name = "Sherpa Wmunu_MAXHTPTV0_70_CFilterBVeto";     m_numEvt = 5.6436e+06;   }
  else if( channelNumber == 364158 ){  m_name = "Sherpa Wmunu_MAXHTPTV0_70_BFilter";          m_numEvt = 1.0403e+07;   }
  else if( channelNumber == 364159 ){  m_name = "Sherpa Wmunu_MAXHTPTV70_140_CVetoBVeto";     m_numEvt = 5.4184e+06;   }
  else if( channelNumber == 364160 ){  m_name = "Sherpa Wmunu_MAXHTPTV70_140_CFilterBVeto";   m_numEvt = 3.69389e+06;  }
  else if( channelNumber == 364161 ){  m_name = "Sherpa Wmunu_MAXHTPTV70_140_BFilter";        m_numEvt = 7.99008e+06;  }
  else if( channelNumber == 364162 ){  m_name = "Sherpa Wmunu_MAXHTPTV140_280_CVetoBVeto";    m_numEvt = 6.1555e+06;   }
  else if( channelNumber == 364163 ){  m_name = "Sherpa Wmunu_MAXHTPTV140_280_CFilterBVeto";  m_numEvt = 5.26081e+06;  }
  else if( channelNumber == 364164 ){  m_name = "Sherpa Wmunu_MAXHTPTV140_280_BFilter";       m_numEvt = 7.27156e+06;  }
  else if( channelNumber == 364165 ){  m_name = "Sherpa Wmunu_MAXHTPTV280_500_CVetoBVeto";    m_numEvt = 4.32528e+06;  }
  else if( channelNumber == 364166 ){  m_name = "Sherpa Wmunu_MAXHTPTV280_500_CFilterBVeto";  m_numEvt = 2.78397e+06;  }
  else if( channelNumber == 364167 ){  m_name = "Sherpa Wmunu_MAXHTPTV280_500_BFilter";       m_numEvt = 2.83571e+06;  }
  else if( channelNumber == 364168 ){  m_name = "Sherpa Wmunu_MAXHTPTV500_1000";              m_numEvt = 5.9417e+06;   }
  else if( channelNumber == 364169 ){  m_name = "Sherpa Wmunu_MAXHTPTV1000_E_CMS";            m_numEvt = 4.06802e+06;  }
  else if( channelNumber == 364170 ){  m_name = "Sherpa Wenu_MAXHTPTV0_70_CVetoBVeto";        m_numEvt = 1.66152e+07;  }
  else if( channelNumber == 364171 ){  m_name = "Sherpa Wenu_MAXHTPTV0_70_CFilterBVeto";      m_numEvt = 5.64704e+06;  }
  else if( channelNumber == 364172 ){  m_name = "Sherpa Wenu_MAXHTPTV0_70_BFilter";           m_numEvt = 1.04079e+07;  }
  else if( channelNumber == 364173 ){  m_name = "Sherpa Wenu_MAXHTPTV70_140_CVetoBVeto";      m_numEvt = 5.35969e+06;  }
  else if( channelNumber == 364174 ){  m_name = "Sherpa Wenu_MAXHTPTV70_140_CFilterBVeto";    m_numEvt = 3.71479e+06;  }
  else if( channelNumber == 364175 ){  m_name = "Sherpa Wenu_MAXHTPTV70_140_BFilter";         m_numEvt = 3.9804e+06;   }
  else if( channelNumber == 364176 ){  m_name = "Sherpa Wenu_MAXHTPTV140_280_CVetoBVeto";     m_numEvt = 6.15928e+06;  }
  else if( channelNumber == 364177 ){  m_name = "Sherpa Wenu_MAXHTPTV140_280_CFilterBVeto";   m_numEvt = 5.26324e+06;  }
  else if( channelNumber == 364178 ){  m_name = "Sherpa Wenu_MAXHTPTV140_280_BFilter";        m_numEvt = 7.3272e+06;   }
  else if( channelNumber == 364179 ){  m_name = "Sherpa Wenu_MAXHTPTV280_500_CVetoBVeto";     m_numEvt = 4.31236e+06;  }
  else if( channelNumber == 364180 ){  m_name = "Sherpa Wenu_MAXHTPTV280_500_CFilterBVeto";   m_numEvt = 2.77865e+06;  }
  else if( channelNumber == 364181 ){  m_name = "Sherpa Wenu_MAXHTPTV280_500_BFilter";        m_numEvt = 2.83531e+06;  }
  else if( channelNumber == 364182 ){  m_name = "Sherpa Wenu_MAXHTPTV500_1000";               m_numEvt = 6.00327e+06;  }
  else if( channelNumber == 364183 ){  m_name = "Sherpa Wenu_MAXHTPTV1000_E_CMS";             m_numEvt = 4.07524e+06;  }
  else if( channelNumber == 364184 ){  m_name = "Sherpa Wtaunu_MAXHTPTV0_70_CVetoBVeto";      m_numEvt = 1.67264e+07;  }
  else if( channelNumber == 364185 ){  m_name = "Sherpa Wtaunu_MAXHTPTV0_70_CFilterBVeto";    m_numEvt = 5.67152e+06;  }
  else if( channelNumber == 364186 ){  m_name = "Sherpa Wtaunu_MAXHTPTV0_70_BFilter";         m_numEvt = 1.04988e+07;  }
  else if( channelNumber == 364187 ){  m_name = "Sherpa Wtaunu_MAXHTPTV70_140_CVetoBVeto";    m_numEvt = 5.42702e+06;  }
  else if( channelNumber == 364188 ){  m_name = "Sherpa Wtaunu_MAXHTPTV70_140_CFilterBVeto";  m_numEvt = 3.71912e+06;  }
  else if( channelNumber == 364189 ){  m_name = "Sherpa Wtaunu_MAXHTPTV70_140_BFilter";       m_numEvt = 3.96912e+06;  }
  else if( channelNumber == 364190 ){  m_name = "Sherpa Wtaunu_MAXHTPTV140_280_CVetoBVeto";   m_numEvt = 6.16652e+06;  }
  else if( channelNumber == 364191 ){  m_name = "Sherpa Wtaunu_MAXHTPTV140_280_CFilterBVeto"; m_numEvt = 5.21265e+06;  }
  else if( channelNumber == 364192 ){  m_name = "Sherpa Wtaunu_MAXHTPTV140_280_BFilter";      m_numEvt = 7.2916e+06;   }
  else if( channelNumber == 364193 ){  m_name = "Sherpa Wtaunu_MAXHTPTV280_500_CVetoBVeto";   m_numEvt = 4.32285e+06;  }
  else if( channelNumber == 364194 ){  m_name = "Sherpa Wtaunu_MAXHTPTV280_500_CFilterBVeto"; m_numEvt = 2.77231e+06;  }
  else if( channelNumber == 364195 ){  m_name = "Sherpa Wtaunu_MAXHTPTV280_500_BFilter";      m_numEvt = 2.83034e+06;  }
  else if( channelNumber == 364196 ){  m_name = "Sherpa Wtaunu_MAXHTPTV500_1000";             m_numEvt = 5.98304e+06;  }
  else if( channelNumber == 364197 ){  m_name = "Sherpa Wtaunu_MAXHTPTV1000_E_CMS";           m_numEvt = 4.05748e+06;  }

  
  /* Diboson */
  else if( channelNumber == 361063 ){  m_name = "Sherpa llll";                     m_numEvt = 2.13112e+06;              }
  else if( channelNumber == 361064 ){  m_name = "Sherpa lllvSFMinus";              m_numEvt = 439585;                   }
  else if( channelNumber == 361065 ){  m_name = "Sherpa lllvOFMinus";              m_numEvt = 878139;                   }
  else if( channelNumber == 361066 ){  m_name = "Sherpa lllvSFPlus";               m_numEvt = 588399;                   }
  else if( channelNumber == 361067 ){  m_name = "Sherpa lllvOFPlus";               m_numEvt = 1.18079e+06;              }
  else if( channelNumber == 361068 ){  m_name = "Sherpa llvv";                     m_numEvt = 6.80539500000000000e+05;  }
  else if( channelNumber == 361081 ){  m_name = "Sherpa WplvWmqq";                 m_numEvt = 3.59086968750000000e+05;  }
  else if( channelNumber == 361082 ){  m_name = "Sherpa WpqqWmlv";                 m_numEvt = 3.59290875000000000e+05;  }
  else if( channelNumber == 361083 ){  m_name = "Sherpa WlvZqq";                   m_numEvt = 1.88351109375000000e+05;  }
  else if( channelNumber == 361084 ){  m_name = "Sherpa WqqZll";                   m_numEvt = 1.87249980468750000e+04;  }
  else if( channelNumber == 361085 ){  m_name = "Sherpa WqqZvv";                   m_numEvt = 1;                        }
  else if( channelNumber == 361086 ){  m_name = "Sherpa ZqqZll";                   m_numEvt = 6.501814843750000e+04;    }
  else if( channelNumber == 361091 ){  m_name = "Sherpa WplvWmqq_SHv21_improved";  m_numEvt = 963785;                   }
  else if( channelNumber == 361092 ){  m_name = "Sherpa WpqqWmlv_SHv21_improved";  m_numEvt = 964014;                   }
  else if( channelNumber == 361093 ){  m_name = "Sherpa WlvZqq_SHv21_improved";    m_numEvt = 1.01462e+06;              }
  else if( channelNumber == 361094 ){  m_name = "Sherpa WqqZll_SHv21_improved";    m_numEvt = 302171;                   }
  else if( channelNumber == 361095 ){  m_name = "Sherpa WqqZvv_SHv21_improved";    m_numEvt = 743153;                   }
  else if( channelNumber == 361096 ){  m_name = "Sherpa ZqqZll_SHv21_improved";    m_numEvt = 2.65662e+06;              }
  else if( channelNumber == 361097 ){  m_name = "Sherpa ZqqZvv_SHv21_improved";    m_numEvt = 2.98537e+06;              }
  else if( channelNumber == 303014 ){  m_name = "Sherpa VV_evev_50M150";           m_numEvt = 190874;                   }
  else if( channelNumber == 303015 ){  m_name = "Sherpa VV_evev_150M500";          m_numEvt = 48325.5;                  }
  else if( channelNumber == 303016 ){  m_name = "Sherpa VV_evev_500M1000";         m_numEvt = 50666.7;                  }
  else if( channelNumber == 303017 ){  m_name = "Sherpa VV_evev_1000M2000";        m_numEvt = 52837.2;                  }
  else if( channelNumber == 303018 ){  m_name = "Sherpa VV_evev_2000M3000";        m_numEvt = 49733.8;                  }
  else if( channelNumber == 303019 ){  m_name = "Sherpa VV_evev_3000M4000";        m_numEvt = 49628.6;                  }
  else if( channelNumber == 303020 ){  m_name = "Sherpa VV_evev_4000M5000";        m_numEvt = 51188.9;                  }
  else if( channelNumber == 303021 ){  m_name = "Sherpa VV_evev_5000M";            m_numEvt = 48165.3;                  }
  else if( channelNumber == 303046 ){  m_name = "Sherpa VV_muvmuv_50M150";         m_numEvt = 196772;                   }
  else if( channelNumber == 303047 ){  m_name = "Sherpa VV_muvmuv_150M500";        m_numEvt = 46339.9;                  }
  else if( channelNumber == 303048 ){  m_name = "Sherpa VV_muvmuv_500M1000";       m_numEvt = 52872.6;                  }
  else if( channelNumber == 303049 ){  m_name = "Sherpa VV_muvmuv_1000M2000";      m_numEvt = 53715.6;                  }
  else if( channelNumber == 303050 ){  m_name = "Sherpa VV_muvmuv_2000M3000";      m_numEvt = 53990.1;                  }
  else if( channelNumber == 303051 ){  m_name = "Sherpa VV_muvmuv_3000M4000";      m_numEvt = 49672.6;                  }
  else if( channelNumber == 303052 ){  m_name = "Sherpa VV_muvmuv_4000M5000";      m_numEvt = 51255.2;                  }
  else if( channelNumber == 303053 ){  m_name = "Sherpa VV_muvmuv_5000M";          m_numEvt = 50314.8;                  }
  else if( channelNumber == 303488 ){  m_name = "Sherpa VV_evmuv_0M150";           m_numEvt = 245757;                   }
  else if( channelNumber == 303489 ){  m_name = "Sherpa VV_evmuv_150M500";         m_numEvt = 150484;                   }
  else if( channelNumber == 303490 ){  m_name = "Sherpa VV_evmuv_500M1000";        m_numEvt = 148413;                   }
  else if( channelNumber == 303491 ){  m_name = "Sherpa VV_evmuv_1000M2000";       m_numEvt = 149432;                   }
  else if( channelNumber == 303492 ){  m_name = "Sherpa VV_evmuv_2000M3000";       m_numEvt = 145402;                   }
  else if( channelNumber == 303493 ){  m_name = "Sherpa VV_evmuv_3000M4000";       m_numEvt = 150444;                   }
  else if( channelNumber == 303494 ){  m_name = "Sherpa VV_evmuv_4000M5000";       m_numEvt = 147629;                   }
  else if( channelNumber == 303495 ){  m_name = "Sherpa VV_evmuv_M5000";           m_numEvt = 149846;                   }
  else if( channelNumber == 304933 ){  m_name = "Sherpa VV_evtauv_0M150";          m_numEvt = 246723;                   }
  else if( channelNumber == 304934 ){  m_name = "Sherpa VV_evtauv_150M500";        m_numEvt = 149388;                   }
  else if( channelNumber == 304935 ){  m_name = "Sherpa VV_evtauv_500M1000";       m_numEvt = 149437;                   }
  else if( channelNumber == 304936 ){  m_name = "Sherpa VV_evtauv_1000M2000";      m_numEvt = 148439;                   }
  else if( channelNumber == 304937 ){  m_name = "Sherpa VV_evtauv_2000M3000";      m_numEvt = 146327;                   }
  else if( channelNumber == 304938 ){  m_name = "Sherpa VV_evtauv_3000M4000";      m_numEvt = 149334;                   }
  else if( channelNumber == 304939 ){  m_name = "Sherpa VV_evtauv_4000M5000";      m_numEvt = 147413;                   }
  else if( channelNumber == 304940 ){  m_name = "Sherpa VV_evtauv_M5000";          m_numEvt = 144753;                   }
  else if( channelNumber == 304941 ){  m_name = "Sherpa VV_muvtauv_0M150";         m_numEvt = 246773;                   }
  else if( channelNumber == 304942 ){  m_name = "Sherpa VV_muvtauv_150M500";       m_numEvt = 150412;                   }
  else if( channelNumber == 304943 ){  m_name = "Sherpa VV_muvtauv_500M1000";      m_numEvt = 148378;                   }
  else if( channelNumber == 304944 ){  m_name = "Sherpa VV_muvtauv_1000M2000";     m_numEvt = 149446;                   }
  else if( channelNumber == 304945 ){  m_name = "Sherpa VV_muvtauv_2000M3000";     m_numEvt = 147556;                   }
  else if( channelNumber == 304946 ){  m_name = "Sherpa VV_muvtauv_3000M4000";     m_numEvt = 147498;                   }
  else if( channelNumber == 304947 ){  m_name = "Sherpa VV_muvtauv_4000M5000";     m_numEvt = 149565;                   }
  else if( channelNumber == 304948 ){  m_name = "Sherpa VV_muvtauv_M5000";         m_numEvt = 148979;                   }
  else if( channelNumber == 304949 ){  m_name = "Sherpa VV_tauvtauv_0M150";        m_numEvt = 244723;                   }
  else if( channelNumber == 304950 ){  m_name = "Sherpa VV_tauvtauv_150M500";      m_numEvt = 149288;                   }
  else if( channelNumber == 304951 ){  m_name = "Sherpa VV_tauvtauv_500M1000";     m_numEvt = 148502;                   }
  else if( channelNumber == 304952 ){  m_name = "Sherpa VV_tauvtauv_1000M2000";    m_numEvt = 149317;                   }
  else if( channelNumber == 304953 ){  m_name = "Sherpa VV_tauvtauv_2000M3000";    m_numEvt = 150561;                   }
  else if( channelNumber == 304954 ){  m_name = "Sherpa VV_tauvtauv_3000M4000";    m_numEvt = 148041;                   }
  else if( channelNumber == 304955 ){  m_name = "Sherpa VV_tauvtauv_4000M5000";    m_numEvt = 148308;                   }
  else if( channelNumber == 304956 ){  m_name = "Sherpa VV_tauvtauv_M5000";        m_numEvt = 145365;                   }
    
  
  /* QBH */
  else if( channelNumber == 303577 ){  m_name  = "Pythia8 QBH_emu_n6 Mth03000";        m_numEvt = 20000;  }
  else if( channelNumber == 303578 ){  m_name  = "Pythia8 QBH_emu_n6 Mth03500";        m_numEvt = 20000;  }
  else if( channelNumber == 303579 ){  m_name  = "Pythia8 QBH_emu_n6 Mth04000";        m_numEvt = 20000;  }
  else if( channelNumber == 303580 ){  m_name  = "Pythia8 QBH_emu_n6 Mth04500";        m_numEvt = 20000;  }
  else if( channelNumber == 303581 ){  m_name  = "Pythia8 QBH_emu_n6 Mth05000";        m_numEvt = 20000;  }
  else if( channelNumber == 303582 ){  m_name  = "Pythia8 QBH_emu_n6 Mth05500";        m_numEvt = 20000;  }
  else if( channelNumber == 303583 ){  m_name  = "Pythia8 QBH_emu_n6 Mth06000";        m_numEvt = 20000;  }
  else if( channelNumber == 303584 ){  m_name  = "Pythia8 QBH_emu_n6 Mth06500";        m_numEvt = 20000;  }
  else if( channelNumber == 303585 ){  m_name  = "Pythia8 QBH_emu_n6 Mth07000";        m_numEvt = 20000;  }
  else if( channelNumber == 303586 ){  m_name  = "Pythia8 QBH_emu_n6 Mth07500";        m_numEvt = 20000;  }
  else if( channelNumber == 303587 ){  m_name  = "Pythia8 QBH_emu_n6 Mth08000";        m_numEvt = 20000;  }
  else if( channelNumber == 303610 ){  m_name  = "Pythia8 QBH_emu_n1 Mth01000";        m_numEvt = 20000;  }
  else if( channelNumber == 303611 ){  m_name  = "Pythia8 QBH_emu_n1 Mth01500";        m_numEvt = 20000;  }
  else if( channelNumber == 303612 ){  m_name  = "Pythia8 QBH_emu_n1 Mth02000";        m_numEvt = 18000;  }
  else if( channelNumber == 303613 ){  m_name  = "Pythia8 QBH_emu_n1 Mth02500";        m_numEvt = 20000;  }
  else if( channelNumber == 303614 ){  m_name  = "Pythia8 QBH_emu_n1 Mth03000";        m_numEvt = 20000;  }
  else if( channelNumber == 303615 ){  m_name  = "Pythia8 QBH_emu_n1 Mth03500";        m_numEvt = 20000;  }
  else if( channelNumber == 303616 ){  m_name  = "Pythia8 QBH_emu_n1 Mth04000";        m_numEvt = 18000;  }
  else if( channelNumber == 303617 ){  m_name  = "Pythia8 QBH_emu_n1 Mth04500";        m_numEvt = 20000;  }
  else if( channelNumber == 303618 ){  m_name  = "Pythia8 QBH_emu_n1 Mth05000";        m_numEvt = 20000;  }
  else if( channelNumber == 303619 ){  m_name  = "Pythia8 QBH_emu_n1 Mth05500";        m_numEvt = 20000;  }
  else if( channelNumber == 303620 ){  m_name  = "Pythia8 QBH_emu_n1 Mth06000";        m_numEvt = 20000;  }
  else if( channelNumber == 303588 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth03000";    m_numEvt = 20000;  }
  else if( channelNumber == 303589 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth03500";    m_numEvt = 19000;  }
  else if( channelNumber == 303590 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth04000";    m_numEvt = 15000;  }
  else if( channelNumber == 303591 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth04500";    m_numEvt = 20000;  }
  else if( channelNumber == 303592 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth05000";    m_numEvt = 20000;  }
  else if( channelNumber == 303593 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth05500";    m_numEvt = 20000;  }
  else if( channelNumber == 303594 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth06000";    m_numEvt = 20000;  }
  else if( channelNumber == 303595 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth06500";    m_numEvt = 20000;  }
  else if( channelNumber == 303596 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth07000";    m_numEvt = 20000;  }
  else if( channelNumber == 303597 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth07500";    m_numEvt = 20000;  }
  else if( channelNumber == 303598 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth08000";    m_numEvt = 20000;  }
  else if( channelNumber == 303621 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth01000";    m_numEvt = 20000;  }
  else if( channelNumber == 303622 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth01500";    m_numEvt = 20000;  }
  else if( channelNumber == 303623 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth02000";    m_numEvt = 20000;  }
  else if( channelNumber == 303624 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth02500";    m_numEvt = 20000;  }
  else if( channelNumber == 303625 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth03000";    m_numEvt = 20000;  }
  else if( channelNumber == 303626 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth03500";    m_numEvt = 18000;  }
  else if( channelNumber == 303627 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth04000";    m_numEvt = 20000;  }
  else if( channelNumber == 303628 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth04500";    m_numEvt = 20000;  }
  else if( channelNumber == 303629 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth05000";    m_numEvt = 20000;  }
  else if( channelNumber == 303630 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth05500";    m_numEvt = 20000;  }
  else if( channelNumber == 303631 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth06000";    m_numEvt = 20000;  }
  else if( channelNumber == 303599 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth03000";   m_numEvt = 20000;  }
  else if( channelNumber == 303600 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth03500";   m_numEvt = 19000;  }
  else if( channelNumber == 303601 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth04000";   m_numEvt = 20000;  }
  else if( channelNumber == 303602 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth04500";   m_numEvt = 20000;  }
  else if( channelNumber == 303603 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth05000";   m_numEvt = 20000;  }
  else if( channelNumber == 303604 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth05500";   m_numEvt = 20000;  }
  else if( channelNumber == 303605 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth06000";   m_numEvt = 20000;  }
  else if( channelNumber == 303606 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth06500";   m_numEvt = 20000;  }
  else if( channelNumber == 303607 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth07000";   m_numEvt = 20000;  }
  else if( channelNumber == 303608 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth07500";   m_numEvt = 20000;  }
  else if( channelNumber == 303609 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth08000";   m_numEvt = 20000;  }
  else if( channelNumber == 303632 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth01000";   m_numEvt = 20000;  }
  else if( channelNumber == 303633 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth01500";   m_numEvt = 20000;  }
  else if( channelNumber == 303634 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth02000";   m_numEvt = 20000;  }
  else if( channelNumber == 303635 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth02500";   m_numEvt = 20000;  }
  else if( channelNumber == 303636 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth03000";   m_numEvt = 20000;  }
  else if( channelNumber == 303637 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth03500";   m_numEvt = 20000;  }
  else if( channelNumber == 303638 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth04000";   m_numEvt = 20000;  }
  else if( channelNumber == 303639 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth04500";   m_numEvt = 20000;  }
  else if( channelNumber == 303640 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth05000";   m_numEvt = 20000;  }
  else if( channelNumber == 303641 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth05500";   m_numEvt = 19000;  }
  else if( channelNumber == 303642 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth06000";   m_numEvt = 20000;  }
  
  /* Zprime */
  else if( channelNumber == 301954 ){  m_name = "Pythia8 Zprime_emu 0500";     m_numEvt = 20000;  }
  else if( channelNumber == 301955 ){  m_name = "Pythia8 Zprime_emu 0600";     m_numEvt = 20000;  }
  else if( channelNumber == 301956 ){  m_name = "Pythia8 Zprime_emu 0700";     m_numEvt = 20000;  }
  else if( channelNumber == 301957 ){  m_name = "Pythia8 Zprime_emu 0800";     m_numEvt = 20000;  }
  else if( channelNumber == 301958 ){  m_name = "Pythia8 Zprime_emu 0900";     m_numEvt = 20000;  }
  else if( channelNumber == 301959 ){  m_name = "Pythia8 Zprime_emu 1000";     m_numEvt = 20000;  }
  else if( channelNumber == 301960 ){  m_name = "Pythia8 Zprime_emu 1100";     m_numEvt = 20000;  }
  else if( channelNumber == 301961 ){  m_name = "Pythia8 Zprime_emu 1200";     m_numEvt = 20000;  }
  else if( channelNumber == 301962 ){  m_name = "Pythia8 Zprime_emu 1300";     m_numEvt = 20000;  }
  else if( channelNumber == 301963 ){  m_name = "Pythia8 Zprime_emu 1400";     m_numEvt = 20000;  }
  else if( channelNumber == 301964 ){  m_name = "Pythia8 Zprime_emu 1500";     m_numEvt = 20000;  }
  else if( channelNumber == 301965 ){  m_name = "Pythia8 Zprime_emu 1600";     m_numEvt = 20000;  }
  else if( channelNumber == 301966 ){  m_name = "Pythia8 Zprime_emu 1700";     m_numEvt = 20000;  }
  else if( channelNumber == 301967 ){  m_name = "Pythia8 Zprime_emu 1800";     m_numEvt = 20000;  }
  else if( channelNumber == 301968 ){  m_name = "Pythia8 Zprime_emu 1900";     m_numEvt = 20000;  }
  else if( channelNumber == 301969 ){  m_name = "Pythia8 Zprime_emu 2000";     m_numEvt = 20000;  }
  else if( channelNumber == 301970 ){  m_name = "Pythia8 Zprime_emu 2200";     m_numEvt = 20000;  }
  else if( channelNumber == 301971 ){  m_name = "Pythia8 Zprime_emu 2400";     m_numEvt = 20000;  }
  else if( channelNumber == 301972 ){  m_name = "Pythia8 Zprime_emu 2600";     m_numEvt = 20000;  }
  else if( channelNumber == 301973 ){  m_name = "Pythia8 Zprime_emu 2800";     m_numEvt = 20000;  }
  else if( channelNumber == 301974 ){  m_name = "Pythia8 Zprime_emu 3000";     m_numEvt = 20000;  }
  else if( channelNumber == 301975 ){  m_name = "Pythia8 Zprime_emu 3500";     m_numEvt = 20000;  }
  else if( channelNumber == 301976 ){  m_name = "Pythia8 Zprime_emu 4000";     m_numEvt = 20000;  }
  else if( channelNumber == 301977 ){  m_name = "Pythia8 Zprime_emu 4500";     m_numEvt = 19000;  }
  else if( channelNumber == 301978 ){  m_name = "Pythia8 Zprime_emu 5000";     m_numEvt = 20000;  }
  else if( channelNumber == 301979 ){  m_name = "Pythia8 Zprime_etau 500";     m_numEvt = 20000;  }
  else if( channelNumber == 301980 ){  m_name = "Pythia8 Zprime_etau 600";     m_numEvt = 20000;  }
  else if( channelNumber == 301981 ){  m_name = "Pythia8 Zprime_etau 700";     m_numEvt = 20000;  }
  else if( channelNumber == 301982 ){  m_name = "Pythia8 Zprime_etau 800";     m_numEvt = 20000;  }
  else if( channelNumber == 301983 ){  m_name = "Pythia8 Zprime_etau 900";     m_numEvt = 20000;  }
  else if( channelNumber == 301984 ){  m_name = "Pythia8 Zprime_etau 1000";    m_numEvt = 20000;  }
  else if( channelNumber == 301985 ){  m_name = "Pythia8 Zprime_etau 1100";    m_numEvt = 20000;  }
  else if( channelNumber == 301986 ){  m_name = "Pythia8 Zprime_etau 1200";    m_numEvt = 20000;  }
  else if( channelNumber == 301987 ){  m_name = "Pythia8 Zprime_etau 1300";    m_numEvt = 20000;  }
  else if( channelNumber == 301988 ){  m_name = "Pythia8 Zprime_etau 1400";    m_numEvt = 18000;  }
  else if( channelNumber == 301989 ){  m_name = "Pythia8 Zprime_etau 1500";    m_numEvt = 19000;  }
  else if( channelNumber == 301990 ){  m_name = "Pythia8 Zprime_etau 1600";    m_numEvt = 20000;  }
  else if( channelNumber == 301991 ){  m_name = "Pythia8 Zprime_etau 1700";    m_numEvt = 20000;  }
  else if( channelNumber == 301993 ){  m_name = "Pythia8 Zprime_etau 1900";    m_numEvt = 20000;  }
  else if( channelNumber == 301992 ){  m_name = "Pythia8 Zprime_etau 1800";    m_numEvt = 20000;  }
  else if( channelNumber == 301994 ){  m_name = "Pythia8 Zprime_etau 2000";    m_numEvt = 20000;  }
  else if( channelNumber == 301995 ){  m_name = "Pythia8 Zprime_etau 2200";    m_numEvt = 20000;  }
  else if( channelNumber == 301996 ){  m_name = "Pythia8 Zprime_etau 2400";    m_numEvt = 20000;  }
  else if( channelNumber == 301997 ){  m_name = "Pythia8 Zprime_etau 2600";    m_numEvt = 20000;  }
  else if( channelNumber == 301998 ){  m_name = "Pythia8 Zprime_etau 2800";    m_numEvt = 20000;  }
  else if( channelNumber == 301999 ){  m_name = "Pythia8 Zprime_etau 3000";    m_numEvt = 20000;  }
  else if( channelNumber == 302000 ){  m_name = "Pythia8 Zprime_etau 3500";    m_numEvt = 20000;  }
  else if( channelNumber == 302001 ){  m_name = "Pythia8 Zprime_etau 4000";    m_numEvt = 20000;  }
  else if( channelNumber == 302002 ){  m_name = "Pythia8 Zprime_etau 4500";    m_numEvt = 20000;  }
  else if( channelNumber == 302003 ){  m_name = "Pythia8 Zprime_etau 5000";    m_numEvt = 20000;  }
  else if( channelNumber == 302004 ){  m_name = "Pythia8 Zprime_mutau 500";    m_numEvt = 20000;  }
  else if( channelNumber == 302005 ){  m_name = "Pythia8 Zprime_mutau 600";    m_numEvt = 20000;  }
  else if( channelNumber == 302006 ){  m_name = "Pythia8 Zprime_mutau 700";    m_numEvt = 20000;  }
  else if( channelNumber == 302007 ){  m_name = "Pythia8 Zprime_mutau 800";    m_numEvt = 20000;  }
  else if( channelNumber == 302008 ){  m_name = "Pythia8 Zprime_mutau 900";    m_numEvt = 20000;  }
  else if( channelNumber == 302009 ){  m_name = "Pythia8 Zprime_mutau 1000";   m_numEvt = 20000;  }
  else if( channelNumber == 302010 ){  m_name = "Pythia8 Zprime_mutau 1100";   m_numEvt = 20000;  }
  else if( channelNumber == 302011 ){  m_name = "Pythia8 Zprime_mutau 1200";   m_numEvt = 19000;  }
  else if( channelNumber == 302012 ){  m_name = "Pythia8 Zprime_mutau 1300";   m_numEvt = 20000;  }
  else if( channelNumber == 302013 ){  m_name = "Pythia8 Zprime_mutau 1400";   m_numEvt = 17000;  }
  else if( channelNumber == 302014 ){  m_name = "Pythia8 Zprime_mutau 1500";   m_numEvt = 20000;  }
  else if( channelNumber == 302015 ){  m_name = "Pythia8 Zprime_mutau 1600";   m_numEvt = 20000;  }
  else if( channelNumber == 302016 ){  m_name = "Pythia8 Zprime_mutau 1700";   m_numEvt = 20000;  }
  else if( channelNumber == 302017 ){  m_name = "Pythia8 Zprime_mutau 1800";   m_numEvt = 20000;  }
  else if( channelNumber == 302018 ){  m_name = "Pythia8 Zprime_mutau 1900";   m_numEvt = 19000;  }
  else if( channelNumber == 302019 ){  m_name = "Pythia8 Zprime_mutau 2000";   m_numEvt = 20000;  }
  else if( channelNumber == 302020 ){  m_name = "Pythia8 Zprime_mutau 2200";   m_numEvt = 20000;  }
  else if( channelNumber == 302021 ){  m_name = "Pythia8 Zprime_mutau 2400";   m_numEvt = 20000;  }
  else if( channelNumber == 302022 ){  m_name = "Pythia8 Zprime_mutau 2600";   m_numEvt = 20000;  }
  else if( channelNumber == 302023 ){  m_name = "Pythia8 Zprime_mutau 2800";   m_numEvt = 20000;  }
  else if( channelNumber == 302024 ){  m_name = "Pythia8 Zprime_mutau 3000";   m_numEvt = 20000;  }
  else if( channelNumber == 302025 ){  m_name = "Pythia8 Zprime_mutau 3500";   m_numEvt = 19000;  }
  else if( channelNumber == 302026 ){  m_name = "Pythia8 Zprime_mutau 4000";   m_numEvt = 20000;  }
  else if( channelNumber == 302027 ){  m_name = "Pythia8 Zprime_mutau 4500";   m_numEvt = 19000;  }
  else if( channelNumber == 302028 ){  m_name = "Pythia8 Zprime_mutau 5000";   m_numEvt = 20000;  }
  
  /* RPV SVT */
  else if( channelNumber == 402970 ){  m_name = "Madgraph Pythia8 SVT_emu 500";      m_numEvt = 19257.9;  }
  else if( channelNumber == 402971 ){  m_name = "Madgraph Pythia8 SVT_emu 600";      m_numEvt = 20222.5;  }
  else if( channelNumber == 402972 ){  m_name = "Madgraph Pythia8 SVT_emu 700";      m_numEvt = 19188.5;  }
  else if( channelNumber == 402973 ){  m_name = "Madgraph Pythia8 SVT_emu 800";      m_numEvt = 20176.3;  }
  else if( channelNumber == 402974 ){  m_name = "Madgraph Pythia8 SVT_emu 900";      m_numEvt = 20152;    }
  else if( channelNumber == 402975 ){  m_name = "Madgraph Pythia8 SVT_emu 1000";     m_numEvt = 20148.6;  }
  else if( channelNumber == 402976 ){  m_name = "Madgraph Pythia8 SVT_emu 1100";     m_numEvt = 20132;    }
  else if( channelNumber == 402977 ){  m_name = "Madgraph Pythia8 SVT_emu 1200";     m_numEvt = 20112.8;  }
  else if( channelNumber == 402978 ){  m_name = "Madgraph Pythia8 SVT_emu 1300";     m_numEvt = 20101;    }
  else if( channelNumber == 402979 ){  m_name = "Madgraph Pythia8 SVT_emu 1400";     m_numEvt = 20100.2;  }
  else if( channelNumber == 402980 ){  m_name = "Madgraph Pythia8 SVT_emu 1500";     m_numEvt = 20094.7;  }
  else if( channelNumber == 402981 ){  m_name = "Madgraph Pythia8 SVT_emu 1600";     m_numEvt = 20084.4;  }
  else if( channelNumber == 402982 ){  m_name = "Madgraph Pythia8 SVT_emu 1700";     m_numEvt = 20081.7;  }
  else if( channelNumber == 402983 ){  m_name = "Madgraph Pythia8 SVT_emu 1800";     m_numEvt = 20071;    }
  else if( channelNumber == 402984 ){  m_name = "Madgraph Pythia8 SVT_emu 1900";     m_numEvt = 20065.8;  }
  else if( channelNumber == 402985 ){  m_name = "Madgraph Pythia8 SVT_emu 2000";     m_numEvt = 20058.9;  }
  else if( channelNumber == 402986 ){  m_name = "Madgraph Pythia8 SVT_emu 2200";     m_numEvt = 20073.8;  }
  else if( channelNumber == 402987 ){  m_name = "Madgraph Pythia8 SVT_emu 2400";     m_numEvt = 19070.6;  }
  else if( channelNumber == 402988 ){  m_name = "Madgraph Pythia8 SVT_emu 2600";     m_numEvt = 20088;    }
  else if( channelNumber == 402989 ){  m_name = "Madgraph Pythia8 SVT_emu 2800";     m_numEvt = 20102.3;  }
  else if( channelNumber == 402990 ){  m_name = "Madgraph Pythia8 SVT_emu 3000";     m_numEvt = 20109.3;  }
  else if( channelNumber == 402991 ){  m_name = "Madgraph Pythia8 SVT_emu 3500";     m_numEvt = 20123.7;  }
  else if( channelNumber == 402992 ){  m_name = "Madgraph Pythia8 SVT_emu 4000";     m_numEvt = 20147.6;  }
  else if( channelNumber == 402993 ){  m_name = "Madgraph Pythia8 SVT_emu 4500";     m_numEvt = 20168.3;  }
  else if( channelNumber == 402994 ){  m_name = "Madgraph Pythia8 SVT_emu 5000";     m_numEvt = 20164.5;  }
  else if( channelNumber == 402995 ){  m_name = "Madgraph Pythia8 SVT_etau 500";     m_numEvt = 20254.5;  }
  else if( channelNumber == 402996 ){  m_name = "Madgraph Pythia8 SVT_etau 600";     m_numEvt = 19212.1;  }
  else if( channelNumber == 402997 ){  m_name = "Madgraph Pythia8 SVT_etau 700";     m_numEvt = 20187.8;  }
  else if( channelNumber == 402998 ){  m_name = "Madgraph Pythia8 SVT_etau 800";     m_numEvt = 20165.4;  }
  else if( channelNumber == 402999 ){  m_name = "Madgraph Pythia8 SVT_etau 900";     m_numEvt = 20158.6;  }
  else if( channelNumber == 403000 ){  m_name = "Madgraph Pythia8 SVT_etau 1000";    m_numEvt = 20139.8;  }
  else if( channelNumber == 403001 ){  m_name = "Madgraph Pythia8 SVT_etau 1100";    m_numEvt = 20131.5;  }
  else if( channelNumber == 403002 ){  m_name = "Madgraph Pythia8 SVT_etau 1200";    m_numEvt = 20115.2;  }
  else if( channelNumber == 403003 ){  m_name = "Madgraph Pythia8 SVT_etau 1300";    m_numEvt = 20108.3;  }
  else if( channelNumber == 403004 ){  m_name = "Madgraph Pythia8 SVT_etau 1400";    m_numEvt = 20104;    }
  else if( channelNumber == 403005 ){  m_name = "Madgraph Pythia8 SVT_etau 1500";    m_numEvt = 20081.5;  }
  else if( channelNumber == 403006 ){  m_name = "Madgraph Pythia8 SVT_etau 1600";    m_numEvt = 20081.8;  }
  else if( channelNumber == 403007 ){  m_name = "Madgraph Pythia8 SVT_etau 1700";    m_numEvt = 20071.3;  }
  else if( channelNumber == 403008 ){  m_name = "Madgraph Pythia8 SVT_etau 1800";    m_numEvt = 19068.9;  }
  else if( channelNumber == 403009 ){  m_name = "Madgraph Pythia8 SVT_etau 1900";    m_numEvt = 20065.2;  }
  else if( channelNumber == 403010 ){  m_name = "Madgraph Pythia8 SVT_etau 2000";    m_numEvt = 20056.7;  }
  else if( channelNumber == 403011 ){  m_name = "Madgraph Pythia8 SVT_etau 2200";    m_numEvt = 20064.2;  }
  else if( channelNumber == 403012 ){  m_name = "Madgraph Pythia8 SVT_etau 2400";    m_numEvt = 20088.4;  }
  else if( channelNumber == 403013 ){  m_name = "Madgraph Pythia8 SVT_etau 2600";    m_numEvt = 20098;    }
  else if( channelNumber == 403014 ){  m_name = "Madgraph Pythia8 SVT_etau 2800";    m_numEvt = 20103.4;  }
  else if( channelNumber == 403015 ){  m_name = "Madgraph Pythia8 SVT_etau 3000";    m_numEvt = 20109.3;  }
  else if( channelNumber == 403016 ){  m_name = "Madgraph Pythia8 SVT_etau 3500";    m_numEvt = 20128.4;  }
  else if( channelNumber == 403017 ){  m_name = "Madgraph Pythia8 SVT_etau 4000";    m_numEvt = 20145;    }
  else if( channelNumber == 403018 ){  m_name = "Madgraph Pythia8 SVT_etau 4500";    m_numEvt = 20154.9;  }
  else if( channelNumber == 403019 ){  m_name = "Madgraph Pythia8 SVT_etau 5000";    m_numEvt = 20178.4;  }
  else if( channelNumber == 403020 ){  m_name = "Madgraph Pythia8 SVT_mutau 500";    m_numEvt = 20258.7;  }
  else if( channelNumber == 403021 ){  m_name = "Madgraph Pythia8 SVT_mutau 600";    m_numEvt = 19206.5;  }
  else if( channelNumber == 403022 ){  m_name = "Madgraph Pythia8 SVT_mutau 700";    m_numEvt = 20205.2;  }
  else if( channelNumber == 403023 ){  m_name = "Madgraph Pythia8 SVT_mutau 800";    m_numEvt = 20166;    }
  else if( channelNumber == 403024 ){  m_name = "Madgraph Pythia8 SVT_mutau 900";    m_numEvt = 19141.3;  }
  else if( channelNumber == 403025 ){  m_name = "Madgraph Pythia8 SVT_mutau 1000";   m_numEvt = 20142.3;  }
  else if( channelNumber == 403026 ){  m_name = "Madgraph Pythia8 SVT_mutau 1100";   m_numEvt = 20132.8;  }
  else if( channelNumber == 403027 ){  m_name = "Madgraph Pythia8 SVT_mutau 1200";   m_numEvt = 19123;    }
  else if( channelNumber == 403028 ){  m_name = "Madgraph Pythia8 SVT_mutau 1300";   m_numEvt = 20115.4;  }
  else if( channelNumber == 403029 ){  m_name = "Madgraph Pythia8 SVT_mutau 1400";   m_numEvt = 20098.4;  }
  else if( channelNumber == 403030 ){  m_name = "Madgraph Pythia8 SVT_mutau 1500";   m_numEvt = 20087;    }
  else if( channelNumber == 403031 ){  m_name = "Madgraph Pythia8 SVT_mutau 1600";   m_numEvt = 19079.5;  }
  else if( channelNumber == 403032 ){  m_name = "Madgraph Pythia8 SVT_mutau 1700";   m_numEvt = 20076.3;  }
  else if( channelNumber == 403033 ){  m_name = "Madgraph Pythia8 SVT_mutau 1800";   m_numEvt = 20072.7;  }
  else if( channelNumber == 403034 ){  m_name = "Madgraph Pythia8 SVT_mutau 1900";   m_numEvt = 20064.9;  }
  else if( channelNumber == 403035 ){  m_name = "Madgraph Pythia8 SVT_mutau 2000";   m_numEvt = 20050.6;  }
  else if( channelNumber == 403036 ){  m_name = "Madgraph Pythia8 SVT_mutau 2200";   m_numEvt = 20068;    }
  else if( channelNumber == 403037 ){  m_name = "Madgraph Pythia8 SVT_mutau 2400";   m_numEvt = 20084.6;  }
  else if( channelNumber == 403038 ){  m_name = "Madgraph Pythia8 SVT_mutau 2600";   m_numEvt = 20092.2;  }
  else if( channelNumber == 403039 ){  m_name = "Madgraph Pythia8 SVT_mutau 2800";   m_numEvt = 20103.1;  }
  else if( channelNumber == 403040 ){  m_name = "Madgraph Pythia8 SVT_mutau 3000";   m_numEvt = 20108.9;  }
  else if( channelNumber == 403041 ){  m_name = "Madgraph Pythia8 SVT_mutau 3500";   m_numEvt = 20124.3;  }
  else if( channelNumber == 403042 ){  m_name = "Madgraph Pythia8 SVT_mutau 4000";   m_numEvt = 20135.3;  }
  else if( channelNumber == 403043 ){  m_name = "Madgraph Pythia8 SVT_mutau 4500";   m_numEvt = 20150.5;  }
  else if( channelNumber == 403044 ){  m_name = "Madgraph Pythia8 SVT_mutau 5000";   m_numEvt = 20152.5;  }
  
  
  if( m_printInfo ){
    std::cout << "----------------------------------------------------" << std::endl;
    std::cout << "     Events (Sum Event wgt) summary:                " << std::endl;
    std::cout << "                                                    " << std::endl;
    std::cout << "           Channel Number  = " << channelNumber       << std::endl;
    std::cout << "           process         = " << m_name              << std::endl;
    if( m_numEvt!=-1 ){
    std::cout << "           Events          = " << m_numEvt            << std::endl;
    }		   
    else{	   
      std::cout << "         NO Number of Events -> set to -1 !"        << std::endl;
    }
  }
  
  return m_numEvt;
  
}

double MCSampleInfo :: GetNumberEvents_14TeV(int channelNumber ){

  m_numEvt = -1;

  if( m_printInfo ){
    std::cout << "----------------------------------------------------" << std::endl;
    std::cout << "     Events (Sum Event wgt) summary:                " << std::endl;
    std::cout << "                                                    " << std::endl;
    std::cout << "           Channel Number  = " << channelNumber       << std::endl;
    std::cout << "           process         = " << m_name              << std::endl;
    if( m_numEvt!=-1){
      std::cout << "           Events          = " << m_numEvt            << std::endl;
    }
    else{
      std::cout << "         NO Number of Events -> set to -1 !"        << std::endl;
    }
  }

  return m_numEvt;
    
}

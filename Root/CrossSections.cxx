#include <LFVUtils/MCSampleInfo.h>

using namespace LFVUtils;

double MCSampleInfo :: GetCrossSection_13TeV(int channelNumber){

  m_crossSection = -1;
  
  /* Zee and DYee */
  if( channelNumber == 361106 ){       m_name = "Powheg+Pythia Zee";                  m_crossSection = 1901.2*1.026*fb;  }
  else if( channelNumber == 301540 ){  m_name = "Pythia8 DYee 70M120";                m_crossSection = 1426.0*fb;        }
  else if( channelNumber == 301541 ){  m_name = "Pythia8 DYee 120M120";               m_crossSection = 13.836*fb;        }
  else if( channelNumber == 301542 ){  m_name = "Pythia8 DYee 180M250";               m_crossSection = 2.3332*fb;        }
  else if( channelNumber == 301543 ){  m_name = "Pythia8 DYee 250M400";               m_crossSection = 0.86745*fb;       }
  else if( channelNumber == 301544 ){  m_name = "Pythia8 DYee 400M600";               m_crossSection = 0.15568*fb;       }
  else if( channelNumber == 301545 ){  m_name = "Pythia8 DYee 600M800";               m_crossSection = 0.02971*fb;       }
  else if( channelNumber == 301546 ){  m_name = "Pythia8 DYee 800M1000";              m_crossSection = 0.0083465*fb;     }
  else if( channelNumber == 301547 ){  m_name = "Pythia8 DYee 1000M1250";             m_crossSection = 0.0033105*fb;     }
  else if( channelNumber == 301548 ){  m_name = "Pythia8 DYee 1250M1500";             m_crossSection = 0.0010982*fb;     }
  else if( channelNumber == 301549 ){  m_name = "Pythia8 DYee 1500M1750";             m_crossSection = 0.00041872*fb;    }
  else if( channelNumber == 301550 ){  m_name = "Pythia8 DYee 1750M2000";             m_crossSection = 0.00017595*fb;    }
  else if( channelNumber == 301551 ){  m_name = "Pythia8 DYee 2000M2250";             m_crossSection = 7.9961e-05*fb;    }
  else if( channelNumber == 301552 ){  m_name = "Pythia8 DYee 2250M2500";             m_crossSection = 3.8234e-05*fb;    }
  else if( channelNumber == 301553 ){  m_name = "Pythia8 DYee 2500M2750";             m_crossSection = 1.9048e-05*fb;    }
  else if( channelNumber == 301554 ){  m_name = "Pythia8 DYee 2750M3000";             m_crossSection = 9.8535e-06*fb;    }
  else if( channelNumber == 301555 ){  m_name = "Pythia8 DYee 3000M3500";             m_crossSection = 8.0449e-06*fb;    }
  else if( channelNumber == 301556 ){  m_name = "Pythia8 DYee 2500M4000";             m_crossSection = 2.419e-06*fb;     }
  else if( channelNumber == 301557 ){  m_name = "Pythia8 DYee 4000M4500";             m_crossSection = 7.5769e-07*fb;    }
  else if( channelNumber == 301558 ){  m_name = "Pythia8 DYee 4500M5000";             m_crossSection = 2.4326e-07*fb;    }
  else if( channelNumber == 301559 ){  m_name = "Pythia8 DYee 5000M";                 m_crossSection = 1.1667e-07*fb;    }
  else if( channelNumber == 301000 ){  m_name = "PowhegPythia8 DYee 120M180";         m_crossSection = 17.478*fb;        }
  else if( channelNumber == 301001 ){  m_name = "PowhegPythia8 DYee 180M250";         m_crossSection = 2.9212*fb;        }
  else if( channelNumber == 301002 ){  m_name = "PowhegPythia8 DYee 250M400";         m_crossSection = 1.082*fb;         }
  else if( channelNumber == 301003 ){  m_name = "PowhegPythia8 DYee 400M600";         m_crossSection = 0.1955*fb;        }
  else if( channelNumber == 301004 ){  m_name = "PowhegPythia8 DYee 600M800";         m_crossSection = 0.037401*fb;      }
  else if( channelNumber == 301005 ){  m_name = "PowhegPythia8 DYee 800M1000";        m_crossSection = 0.010607*fb;      }
  else if( channelNumber == 301006 ){  m_name = "PowhegPythia8 DYee 1000M1250";       m_crossSection = 0.0042582*fb;     }     
  else if( channelNumber == 301007 ){  m_name = "PowhegPythia8 DYee 1250M1500";       m_crossSection = 0.0014219*fb;     }
  else if( channelNumber == 301008 ){  m_name = "PowhegPythia8 DYee 1500M1750";       m_crossSection = 0.00054521*fb;    }
  else if( channelNumber == 301009 ){  m_name = "PowhegPythia8 DYee 1750M2000";       m_crossSection = 0.00022992*fb;    }
  else if( channelNumber == 301010 ){  m_name = "PowhegPythia8 DYee 2000M2250";       m_crossSection = 0.00010387*fb;    }
  else if( channelNumber == 301011 ){  m_name = "PowhegPythia8 DYee 2250M2500";       m_crossSection = 4.94e-05*fb;      }
  else if( channelNumber == 301012 ){  m_name = "PowhegPythia8 DYee 2500M2750";       m_crossSection = 2.4452e-05*fb;    }
  else if( channelNumber == 301013 ){  m_name = "PowhegPythia8 DYee 2750M3000";       m_crossSection = 1.2487e-05*fb;    }
  else if( channelNumber == 301014 ){  m_name = "PowhegPythia8 DYee 3000M3500";       m_crossSection = 1.0029e-05*fb;    }
  else if( channelNumber == 301015 ){  m_name = "PowhegPythia8 DYee 3500M4000";       m_crossSection = 2.9342e-06*fb;    }
  else if( channelNumber == 301016 ){  m_name = "PowhegPythia8 DYee 4000M4500";       m_crossSection = 8.9764e-07*fb;    }
  else if( channelNumber == 301017 ){  m_name = "PowhegPythia8 DYee 4500M5000";       m_crossSection = 2.8071e-07*fb;    }
  else if( channelNumber == 301018 ){  m_name = "PowhegPythia8 DYee 5000M";           m_crossSection = 1.2649e-07*fb;    }
  
  
  /* Zmumu and DYmumu */
  else if( channelNumber == 361107 ){  m_name = "Powheg+Pythia Zmumu";                m_crossSection = 1901.2*1.026*fb;  } 
  else if( channelNumber == 301560 ){  m_name = "Pythia8 DYmumu 70M120";              m_crossSection = 1430.1*fb;        }
  else if( channelNumber == 301561 ){  m_name = "Pythia8 DYmumu 120M180";             m_crossSection = 13.846*fb;        }
  else if( channelNumber == 301562 ){  m_name = "Pythia8 DYmumu 180M250";             m_crossSection = 2.3311*fb;        }
  else if( channelNumber == 301563 ){  m_name = "Pythia8 DYmumu 350M400";             m_crossSection = 0.86684*fb;       }
  else if( channelNumber == 301564 ){  m_name = "Pythia8 DYmumu 400M600";             m_crossSection = 0.15621*fb;       }
  else if( channelNumber == 301565 ){  m_name = "Pythia8 DYmumu 600M800";             m_crossSection = 0.029571*fb;      }
  else if( channelNumber == 301566 ){  m_name = "Pythia8 DYmumu 800M1000";            m_crossSection = 0.0083307*fb;     }
  else if( channelNumber == 301567 ){  m_name = "Pythia8 DYmumu 1000M1250";           m_crossSection = 0.0033162*fb;     }
  else if( channelNumber == 301568 ){  m_name = "Pythia8 DYmumu 1250M1500";           m_crossSection = 0.001098*fb;      }
  else if( channelNumber == 301569 ){  m_name = "Pythia8 DYmumu 1500M1750";           m_crossSection = 0.00041839*fb;    }
  else if( channelNumber == 301570 ){  m_name = "Pythia8 DYmumu 1750M2000";           m_crossSection = 0.0001762*fb;     }
  else if( channelNumber == 301571 ){  m_name = "Pythia8 DYmumu 2000M2250";           m_crossSection = 7.9698e-05*fb;    }
  else if( channelNumber == 301572 ){  m_name = "Pythia8 DYmumu 2250M2500";           m_crossSection = 3.8193e-05*fb;    }
  else if( channelNumber == 301573 ){  m_name = "Pythia8 DYmumu 2500M2750";           m_crossSection = 1.9148e-05*fb;    }
  else if( channelNumber == 301574 ){  m_name = "Pythia8 DYmumu 2750M3000";           m_crossSection = 9.8945e-06*fb;    }
  else if( channelNumber == 301575 ){  m_name = "Pythia8 DYmumu 3000M3500";           m_crossSection = 8.0138e-06*fb;    }
  else if( channelNumber == 301576 ){  m_name = "Pythia8 DYmumu 3500M4000";           m_crossSection = 2.4223e-06*fb;    }
  else if( channelNumber == 301577 ){  m_name = "Pythia8 DYmumu 4000M4500";           m_crossSection = 7.5614e-07*fb;    }
  else if( channelNumber == 301578 ){  m_name = "Pythia8 DYmumu 4500M5000";           m_crossSection = 2.4256e-07*fb;    }
  else if( channelNumber == 301579 ){  m_name = "Pythia8 DYmumu 5000M";               m_crossSection = 1.1636e-07*fb;    }
  else if( channelNumber == 301020 ){  m_name = "PowhegPythia8 DYmumu 120M180";       m_crossSection = 17.478*fb;        }
  else if( channelNumber == 301021 ){  m_name = "PowhegPythia8 DYmumu 180M250";       m_crossSection = 2.9212*fb;        }
  else if( channelNumber == 301022 ){  m_name = "PowhegPythia8 DYmumu 250M400";       m_crossSection = 1.082*fb;         }
  else if( channelNumber == 301023 ){  m_name = "PowhegPythia8 DYmumu 400M600";       m_crossSection = 0.1955*fb;        }
  else if( channelNumber == 301024 ){  m_name = "PowhegPythia8 DYmumu 600M800";       m_crossSection = 0.037401*fb;      }
  else if( channelNumber == 301025 ){  m_name = "PowhegPythia8 DYmumu 800M1000";      m_crossSection = 0.010607*fb;      }
  else if( channelNumber == 301026 ){  m_name = "PowhegPythia8 DYmumu 1000M1250";     m_crossSection = 0.0042582*fb;     }
  else if( channelNumber == 301027 ){  m_name = "PowhegPythia8 DYmumu 1250M1500";     m_crossSection = 0.0014219*fb;     }
  else if( channelNumber == 301028 ){  m_name = "PowhegPythia8 DYmumu 1500M1750";     m_crossSection = 0.00054521*fb;    }
  else if( channelNumber == 301029 ){  m_name = "PowhegPythia8 DYmumu 1750M2000";     m_crossSection = 0.00022992*fb;    }
  else if( channelNumber == 301030 ){  m_name = "PowhegPythia8 DYmumu 2000M2250";     m_crossSection = 0.00010387*fb;    }
  else if( channelNumber == 301031 ){  m_name = "PowhegPythia8 DYmumu 2250M2500";     m_crossSection = 4.94e-05*fb;      }
  else if( channelNumber == 301032 ){  m_name = "PowhegPythia8 DYmumu 2500M2750";     m_crossSection = 2.4452e-05*fb;    }
  else if( channelNumber == 301033 ){  m_name = "PowhegPythia8 DYmumu 2750M3000";     m_crossSection = 1.2487e-05*fb;    }
  else if( channelNumber == 301034 ){  m_name = "PowhegPythia8 DYmumu 3000M3500";     m_crossSection = 1.0029e-05*fb;    }
  else if( channelNumber == 301035 ){  m_name = "PowhegPythia8 DYmumu 3500M4000";     m_crossSection = 2.9342e-06*fb;    }
  else if( channelNumber == 301036 ){  m_name = "PowhegPythia8 DYmumu 4000M4500";     m_crossSection = 8.9764e-07*fb;    }
  else if( channelNumber == 301037 ){  m_name = "PowhegPythia8 DYmumu 4500M5000";     m_crossSection = 2.8071e-07*fb;    }
  else if( channelNumber == 301038 ){  m_name = "PowhegPythia8 DYmumu 5000M";         m_crossSection = 1.2649e-07*fb;    }
  
  
  /* Ztautau and DYtautau */
  else if( channelNumber == 361108 ){  m_name = "Powheg+Pythia Ztautau";              m_crossSection = 1901.2*1.026*fb;  }
  else if( channelNumber == 303437 ){  m_name = "Pythia8 DYtautau 120M180";           m_crossSection = 13.842*fb;        }
  else if( channelNumber == 303438 ){  m_name = "Pythia8 DYtautau 180M250";           m_crossSection = 2.3352*fb;        }
  else if( channelNumber == 303439 ){  m_name = "Pythia8 DYtautau 250M400";           m_crossSection = 0.86526*fb;       }
  else if( channelNumber == 303440 ){  m_name = "Pythia8 DYtautau 400M600";           m_crossSection = 0.15594*fb;       }
  else if( channelNumber == 303441 ){  m_name = "Pythia8 DYtautau 600M800";           m_crossSection = 0.029643*fb;      }
  else if( channelNumber == 303442 ){  m_name = "Pythia8 DYtautau 800M1000";          m_crossSection = 0.0083148*fb;     }
  else if( channelNumber == 303443 ){  m_name = "Pythia8 DYtautau 1000M1250";         m_crossSection = 0.0033072*fb;     }
  else if( channelNumber == 303444 ){  m_name = "Pythia8 DYtautau 1250M1500";         m_crossSection = 0.0010955*fb;     }
  else if( channelNumber == 303445 ){  m_name = "Pythia8 DYtautau 1500M1750";         m_crossSection = 0.00041817*fb;    }
  else if( channelNumber == 303446 ){  m_name = "Pythia8 DYtautau 1750M2000";         m_crossSection = 0.0001761*fb;     }
  else if( channelNumber == 303447 ){  m_name = "Pythia8 DYtautau 2000M2250";         m_crossSection = 7.9838e-05*fb;    }
  else if( channelNumber == 303448 ){  m_name = "Pythia8 DYtautau 2250M2500";         m_crossSection = 3.8223e-05*fb;    }
  else if( channelNumber == 303449 ){  m_name = "Pythia8 DYtautau 2500M2750";         m_crossSection = 1.9088e-05*fb;    }
  else if( channelNumber == 303450 ){  m_name = "Pythia8 DYtautau 2750M3000";         m_crossSection = 9.8673e-06*fb;    }
  else if( channelNumber == 303451 ){  m_name = "Pythia8 DYtautau 3000M3500";         m_crossSection = 8.0521e-06*fb;    }
  else if( channelNumber == 303452 ){  m_name = "Pythia8 DYtautau 3500M4000";         m_crossSection = 2.4178e-06*fb;    }
  else if( channelNumber == 303453 ){  m_name = "Pythia8 DYtautau 4000M4500";         m_crossSection = 7.5696e-07*fb;    }
  else if( channelNumber == 303454 ){  m_name = "Pythia8 DYtautau 4500M5000";         m_crossSection = 2.4277e-07*fb;    }
  else if( channelNumber == 303455 ){  m_name = "Pythia8 DYtautau 5000M";             m_crossSection = 1.1662e-07*fb;    }  
  else if( channelNumber == 301040 ){  m_name = "PowhegPythia8 DYtautau 120M180";     m_crossSection = 17.479*fb;        }
  else if( channelNumber == 301041 ){  m_name = "PowhegPythia8 DYtautau 180M250";     m_crossSection = 2.9213*fb;        }
  else if( channelNumber == 301042 ){  m_name = "PowhegPythia8 DYtautau 250M400";     m_crossSection = 1.082*fb;         }
  else if( channelNumber == 301043 ){  m_name = "PowhegPythia8 DYtautau 400M600";     m_crossSection = 0.1955*fb;        }
  else if( channelNumber == 301044 ){  m_name = "PowhegPythia8 DYtautau 600M800";     m_crossSection = 0.037401*fb;      }
  else if( channelNumber == 301045 ){  m_name = "PowhegPythia8 DYtautau 800M1000";    m_crossSection = 0.010607*fb;      }
  else if( channelNumber == 301046 ){  m_name = "PowhegPythia8 DYtautau 1000M1250";   m_crossSection = 0.0042585*fb;     }
  else if( channelNumber == 301047 ){  m_name = "PowhegPythia8 DYtautau 1250M1500";   m_crossSection = 0.001422*fb;      }
  else if( channelNumber == 301048 ){  m_name = "PowhegPythia8 DYtautau 1500M1750";   m_crossSection = 0.00054524*fb;    }
  else if( channelNumber == 301049 ){  m_name = "PowhegPythia8 DYtautau 1750M2000";   m_crossSection = 0.00022992*fb;    }
  else if( channelNumber == 301050 ){  m_name = "PowhegPythia8 DYtautau 2000M2250";   m_crossSection = 0.00010386*fb;    }
  else if( channelNumber == 301051 ){  m_name = "PowhegPythia8 DYtautau 2250M2500";   m_crossSection = 4.940e-05*fb;     }
  else if( channelNumber == 301052 ){  m_name = "PowhegPythia8 DYtautau 2500M2750";   m_crossSection = 2.4454e-05*fb;    }
  else if( channelNumber == 301053 ){  m_name = "PowhegPythia8 DYtautau 2750M3000";   m_crossSection = 1.249e-05*fb;     }
  else if( channelNumber == 301054 ){  m_name = "PowhegPythia8 DYtautau 3000M3500";   m_crossSection = 1.0031e-05*fb;    }
  else if( channelNumber == 301055 ){  m_name = "PowhegPythia8 DYtautau 3500M4000";   m_crossSection = 2.9343e-06*fb;    }
  else if( channelNumber == 301056 ){  m_name = "PowhegPythia8 DYtautau 4000M4500";   m_crossSection = 8.9766e-07*fb;    }
  else if( channelNumber == 301057 ){  m_name = "PowhegPythia8 DYtautau 4500M5000";   m_crossSection = 2.8071e-07*fb;    }
  else if( channelNumber == 301058 ){  m_name = "PowhegPythia8 DYtautau 5000M";       m_crossSection = 1.2648e-07*fb;    }
  
    
  /* ttbar */
  else if( channelNumber == 410000 ){  m_name = "Powheg+Pythia ttbar_hdamp172p5_nonallhad";      m_crossSection = 696.11*0.5442*1.195*fb;     }
  else if( channelNumber == 410001 ){  m_name = "Powheg+Pythia ttbar_hdamp345_down_nonallhad";   m_crossSection = 783.73*0.5442*1.0613*fb;    }
  else if( channelNumber == 410002 ){  m_name = "Powheg+Pythia ttbar_hdamp345_up_nonallhad";     m_crossSection = 611.1*0.5442*1.3611*fb;     }
  else if( channelNumber == 410003 ){  m_name = "aMcAtNlo+Herwigpp ttbar_nonallhad";             m_crossSection = 694.59*0.5442*1.1975*fb;    }
  else if( channelNumber == 410007 ){  m_name = "Powheg+Pythia ttbar_hdamp172p5_allhad";         m_crossSection = 696.21*0.457*1.195*fb;      }
  else if( channelNumber == 410009 ){  m_name = "Powheg+Pythia ttbar_hdamp172p5_dilepton";       m_crossSection = 696.11*0.10534*1.195*fb;    }
  else if( channelNumber == 410021 ){  m_name = "Sherpa ttbar DiLepton";                         m_crossSection = 78.73*1.17*fb;              }
  else if( channelNumber == 410022 ){  m_name = "Sherpa ttbar SingleLeptonP";                    m_crossSection = 157.35*1.17*fb;             }
  else if( channelNumber == 410023 ){  m_name = "Sherpa ttbar SingleLeptonM";                    m_crossSection = 157.54*1.17*fb;             }
  else if( channelNumber == 410470 ){  m_name = "Powheg+Pythia ttbar_hdamp258p75_nonallhad";     m_crossSection = 729.77*0.54384*1.1398*fb;   }
  else if( channelNumber == 306600 ){  m_name = "Sherpa 221 ttbar_ee_0M150";                     m_crossSection = 4.3271*1.1484*1.37*fb;      }
  else if( channelNumber == 306601 ){  m_name = "Sherpa 221 ttbar_ee_150M250";                   m_crossSection = 0.85325*1.1484*1.37*fb;     }
  else if( channelNumber == 306602 ){  m_name = "Sherpa 221 ttbar_ee_250M500";                   m_crossSection = 0.28511*1.1484*1.37*fb;     }
  else if( channelNumber == 306603 ){  m_name = "Sherpa 221 ttbar_ee_500M1000";                  m_crossSection = 0.021725*1.1484*1.37*fb;    }
  else if( channelNumber == 306604 ){  m_name = "Sherpa 221 ttbar_ee_1000M2000";                 m_crossSection = 0.00060548*1.1484*1.37*fb;  }
  else if( channelNumber == 306605 ){  m_name = "Sherpa 221 ttbar_ee_2000M3000";                 m_crossSection = 4.3551e-06*1.1484*1.37*fb;  }
  else if( channelNumber == 306606 ){  m_name = "Sherpa 221 ttbar_ee_3000M4000";                 m_crossSection = 8.6362e-08*1.1484*1.37*fb;  }
  else if( channelNumber == 306607 ){  m_name = "Sherpa 221 ttbar_ee_4000M5000";                 m_crossSection = 2.4758e-09*1.1484*1.37*fb;  }
  else if( channelNumber == 306608 ){  m_name = "Sherpa 221 ttbar_ee_5000M";                     m_crossSection = 1.0689e-10*1.1484*1.37*fb;  }
  else if( channelNumber == 306609 ){  m_name = "Sherpa 221 ttbar_mumu_0M150";                   m_crossSection = 4.3281*1.1484*1.37*fb;      }
  else if( channelNumber == 306610 ){  m_name = "Sherpa 221 ttbar_mumu_150M250";                 m_crossSection = 0.84689*1.1484*1.37*fb;     }
  else if( channelNumber == 306611 ){  m_name = "Sherpa 221 ttbar_mumu_250M500";                 m_crossSection = 0.28603*1.1484*1.37*fb;     }
  else if( channelNumber == 306612 ){  m_name = "Sherpa 221 ttbar_mumu_500M1000";                m_crossSection = 0.021624*1.1484*1.37*fb;    }
  else if( channelNumber == 306613 ){  m_name = "Sherpa 221 ttbar_mumu_1000M2000";               m_crossSection = 0.00060036*1.1484*1.37*fb;  }
  else if( channelNumber == 306614 ){  m_name = "Sherpa 221 ttbar_mumu_2000M3000";               m_crossSection = 4.3538e-06*1.1484*1.37*fb;  }
  else if( channelNumber == 306615 ){  m_name = "Sherpa 221 ttbar_mumu_3000M4000";               m_crossSection = 8.7675e-08*1.1484*1.37*fb;  }
  else if( channelNumber == 306616 ){  m_name = "Sherpa 221 ttbar_mumu_4000M5000";               m_crossSection = 2.4398e-09*1.1484*1.37*fb;  }
  else if( channelNumber == 306617 ){  m_name = "Sherpa 221 ttbar_mumu_5000M";                   m_crossSection = 1.0239e-10*1.1484*1.37*fb;  }
  else if( channelNumber == 307479 ){  m_name = "Sherpa 221 ttbar_emu_0M150";                    m_crossSection = 8.6019*1.1484*1.37*fb;      }
  else if( channelNumber == 307480 ){  m_name = "Sherpa 221 ttbar_emu_150M500";                  m_crossSection = 2.2525*1.1484*1.37*fb;      }
  else if( channelNumber == 307481 ){  m_name = "Sherpa 221 ttbar_emu_500M1000";                 m_crossSection = 0.043095*1.1484*1.37*fb;    }
  else if( channelNumber == 307482 ){  m_name = "Sherpa 221 ttbar_emu_1000M2000";                m_crossSection = 0.0011984*1.1484*1.37*fb;   }
  else if( channelNumber == 307483 ){  m_name = "Sherpa 221 ttbar_emu_2000M3000";                m_crossSection = 8.6749e-06*1.1484*1.37*fb;  }
  else if( channelNumber == 307484 ){  m_name = "Sherpa 221 ttbar_emu_M3000";                    m_crossSection = 1.7652e-07*1.1484*1.37*fb;  }
  else if( channelNumber == 307485 ){  m_name = "Sherpa 221 ttbar_etau_0M150";                   m_crossSection = 8.5834*1.1484*1.37*fb;      }
  else if( channelNumber == 307486 ){  m_name = "Sherpa 221 ttbar_etau_150M500";                 m_crossSection = 2.2613*1.1484*1.37*fb;      }
  else if( channelNumber == 307487 ){  m_name = "Sherpa 221 ttbar_etau_500M1000";                m_crossSection = 0.043029*1.1484*1.37*fb;    }
  else if( channelNumber == 307488 ){  m_name = "Sherpa 221 ttbar_etau_1000M2000";               m_crossSection = 0.0012034*1.1484*1.37*fb;   }
  else if( channelNumber == 307489 ){  m_name = "Sherpa 221 ttbar_etau_2000M3000";               m_crossSection = 8.6833e-06*1.1484*1.37*fb;  }
  else if( channelNumber == 307490 ){  m_name = "Sherpa 221 ttbar_etau_M3000";                   m_crossSection = 1.7724e-07*1.1484*1.37*fb;  }
  else if( channelNumber == 307491 ){  m_name = "Sherpa 221 ttbar_mutau_0M150";                  m_crossSection = 8.7146*1.1484*1.37*fb;      }
  else if( channelNumber == 307492 ){  m_name = "Sherpa 221 ttbar_mutau_150M500";                m_crossSection = 2.3083*1.1484*1.37*fb;      }
  else if( channelNumber == 307493 ){  m_name = "Sherpa 221 ttbar_mutau_500M1000";               m_crossSection = 0.043187*1.1484*1.37*fb;    }
  else if( channelNumber == 307494 ){  m_name = "Sherpa 221 ttbar_mutau_1000M2000";              m_crossSection = 0.0011951*1.1484*1.37*fb;   }
  else if( channelNumber == 307495 ){  m_name = "Sherpa 221 ttbar_mutau_2000M3000";              m_crossSection = 8.641e-06*1.1484*1.37*fb;   }
  else if( channelNumber == 307496 ){  m_name = "Sherpa 221 ttbar_mutau_M3000";                  m_crossSection = 1.7525e-07*1.1484*1.37*fb;  }
  else if( channelNumber == 307497 ){  m_name = "Sherpa 221 ttbar_tautau_0M150";                 m_crossSection = 4.2565*1.1484*1.37*fb;      }
  else if( channelNumber == 307498 ){  m_name = "Sherpa 221 ttbar_tautau_150M500";               m_crossSection = 1.1286*1.1484*1.37*fb;      }
  else if( channelNumber == 307499 ){  m_name = "Sherpa 221 ttbar_tautau_500M1000";              m_crossSection = 0.021475*1.1484*1.37*fb;    }
  else if( channelNumber == 307500 ){  m_name = "Sherpa 221 ttbar_tautau_1000M2000";             m_crossSection = 0.0006037*1.1484*1.37*fb;   }
  else if( channelNumber == 307501 ){  m_name = "Sherpa 221 ttbar_tautau_2000M3000";             m_crossSection = 4.3395e-06*1.1484*1.37*fb;  }
  else if( channelNumber == 307502 ){  m_name = "Sherpa 221 ttbar_tautau_M3000";                 m_crossSection = 8.9497e-08*1.1484*1.37*fb;  }

  /* ttbar+X */
  else if( channelNumber == 410142 ){  m_name = "Sherpa 22 ttll_mll5";                           m_crossSection = 0.11309*1.09*fb; }
  else if( channelNumber == 410143 ){  m_name = "Sherpa 22 ttZnnqq";                             m_crossSection = 0.6881*1.1*fb;   }
  else if( channelNumber == 410144 ){  m_name = "Sherpa 22 ttW";                                 m_crossSection = 0.57729*1.04*fb; }
  
  /* SingleTop*/
  else if( channelNumber == 410013 ){  m_name = "Powheg+Pythia Wt_inclusive_top";                m_crossSection = 34.009*1.054*fb;  }
  else if( channelNumber == 410014 ){  m_name = "Powheg+Pythia Wt_inclusive_antitop";            m_crossSection = 33.989*1.054*fb;  }
  
  /* W+jets */
  else if( channelNumber == 361100 ){  m_name = "Powheg+Pythia8 Wplusenu";                       m_crossSection = 11306.0*1.0172*fb; }
  else if( channelNumber == 361101 ){  m_name = "Powheg+Pythia8 Wplusmunu";                      m_crossSection = 11306.0*1.0172*fb; }
  else if( channelNumber == 361102 ){  m_name = "Powheg+Pythia8 Wplustaunu";                     m_crossSection = 11306.0*1.0172*fb; }
  else if( channelNumber == 361103 ){  m_name = "Powheg+Pythia8 Wminusenu";                      m_crossSection = 8282.7*1.0358*fb;  }
  else if( channelNumber == 361104 ){  m_name = "Powheg+Pythia8 Wminusmunu";                     m_crossSection = 8282.5*1.0358*fb;  }
  else if( channelNumber == 361105 ){  m_name = "Powheg+Pythia8 Wminustaunu";                    m_crossSection = 8282.6*1.0358*fb;  }
  
  /* W+jets - HT, PTV slices */
  else if( channelNumber == 364156 ){  m_name = "Sherpa Wmunu_MAXHTPTV0_70_CVetoBVeto";          m_crossSection = 19143.0*0.8238*0.9702*fb;   }
  else if( channelNumber == 364157 ){  m_name = "Sherpa Wmunu_MAXHTPTV0_70_CFilterBVeto";        m_crossSection = 19121.0*0.1304*0.9702*fb;   }
  else if( channelNumber == 364158 ){  m_name = "Sherpa Wmunu_MAXHTPTV0_70_BFilter";             m_crossSection = 19135.0*0.044118*0.9702*fb; }
  else if( channelNumber == 364159 ){  m_name = "Sherpa Wmunu_MAXHTPTV70_140_CVetoBVeto";        m_crossSection = 944.85*0.67463*0.9702*fb;   }
  else if( channelNumber == 364160 ){  m_name = "Sherpa Wmunu_MAXHTPTV70_140_CFilterBVeto";      m_crossSection = 937.78*0.23456*0.9702*fb;   }
  else if( channelNumber == 364161 ){  m_name = "Sherpa Wmunu_MAXHTPTV70_140_BFilter";           m_crossSection = 944.63*0.075648*0.9702*fb;  }
  else if( channelNumber == 364162 ){  m_name = "Sherpa Wmunu_MAXHTPTV140_280_CVetoBVeto";       m_crossSection = 339.54*0.62601*0.9702*fb;   }
  else if( channelNumber == 364163 ){  m_name = "Sherpa Wmunu_MAXHTPTV140_280_CFilterBVeto";     m_crossSection = 340.06*0.28947*0.9702*fb;   }
  else if( channelNumber == 364164 ){  m_name = "Sherpa Wmunu_MAXHTPTV140_280_BFilter";          m_crossSection = 339.54*0.10872*0.9702*fb;   }
  else if( channelNumber == 364165 ){  m_name = "Sherpa Wmunu_MAXHTPTV280_500_CVetoBVeto";       m_crossSection = 72.067*0.54647*0.9702*fb;   }
  else if( channelNumber == 364166 ){  m_name = "Sherpa Wmunu_MAXHTPTV280_500_CFilterBVeto";     m_crossSection = 72.198*0.31743*0.9702*fb;   }
  else if( channelNumber == 364167 ){  m_name = "Sherpa Wmunu_MAXHTPTV280_500_BFilter";          m_crossSection = 72.045*0.13337*0.9702*fb;   }
  else if( channelNumber == 364168 ){  m_name = "Sherpa Wmunu_MAXHTPTV500_1000";                 m_crossSection = 15.01*1.0*0.9702*fb;        }
  else if( channelNumber == 364169 ){  m_name = "Sherpa Wmunu_MAXHTPTV1000_E_CMS";               m_crossSection = 1.2344*1.0*0.9702*fb;       }
  else if( channelNumber == 364170 ){  m_name = "Sherpa Wenu_MAXHTPTV0_70_CVetoBVeto";           m_crossSection = 19127.0*0.82447*0.9702*fb;  }
  else if( channelNumber == 364171 ){  m_name = "Sherpa Wenu_MAXHTPTV0_70_CFilterBVeto";         m_crossSection = 19130.0*0.1303*0.9702*fb;   }
  else if( channelNumber == 364172 ){  m_name = "Sherpa Wenu_MAXHTPTV0_70_BFilter";              m_crossSection = 19135.0*0.044141*0.9702*fb; }
  else if( channelNumber == 364173 ){  m_name = "Sherpa Wenu_MAXHTPTV70_140_CVetoBVeto";         m_crossSection = 942.58*0.66872*0.9702*fb;   }
  else if( channelNumber == 364174 ){  m_name = "Sherpa Wenu_MAXHTPTV70_140_CFilterBVeto";       m_crossSection = 945.67*0.22787*0.9702*fb;   }
  else if( channelNumber == 364175 ){  m_name = "Sherpa Wenu_MAXHTPTV70_140_BFilter";            m_crossSection = 945.15*0.10341*0.9702*fb;   }
  else if( channelNumber == 364176 ){  m_name = "Sherpa Wenu_MAXHTPTV140_280_CVetoBVeto";        m_crossSection = 339.81*0.59691*0.9702*fb;   }
  else if( channelNumber == 364177 ){  m_name = "Sherpa Wenu_MAXHTPTV140_280_CFilterBVeto";      m_crossSection = 339.87*0.28965*0.9702*fb;   }
  else if( channelNumber == 364178 ){  m_name = "Sherpa Wenu_MAXHTPTV140_280_BFilter";           m_crossSection = 339.48*0.10898*0.9702*fb;   }
  else if( channelNumber == 364179 ){  m_name = "Sherpa Wenu_MAXHTPTV280_500_CVetoBVeto";        m_crossSection = 72.084*0.54441*0.9702*fb;   }
  else if( channelNumber == 364180 ){  m_name = "Sherpa Wenu_MAXHTPTV280_500_CFilterBVeto";      m_crossSection = 72.128*0.31675*0.9702*fb;   }
  else if( channelNumber == 364181 ){  m_name = "Sherpa Wenu_MAXHTPTV280_500_BFilter";           m_crossSection = 72.113*0.13391*0.9702*fb;   }
  else if( channelNumber == 364182 ){  m_name = "Sherpa Wenu_MAXHTPTV500_1000";                  m_crossSection = 15.224*1.0*0.9702*fb;       }
  else if( channelNumber == 364183 ){  m_name = "Sherpa Wenu_MAXHTPTV1000_E_CMS";                m_crossSection = 1.2334*1.0*0.9702*fb;       }
  else if( channelNumber == 364184 ){  m_name = "Sherpa Wtaunu_MAXHTPTV0_70_CVetoBVeto";         m_crossSection = 19152.0*0.82495*0.9702*fb;  }
  else if( channelNumber == 364185 ){  m_name = "Sherpa Wtaunu_MAXHTPTV0_70_CFilterBVeto";       m_crossSection = 19153.0*0.12934*0.9702*fb;  }
  else if( channelNumber == 364186 ){  m_name = "Sherpa Wtaunu_MAXHTPTV0_70_BFilter";            m_crossSection = 19163.0*0.044594*0.9702*fb; }
  else if( channelNumber == 364187 ){  m_name = "Sherpa Wtaunu_MAXHTPTV70_140_CVetoBVeto";       m_crossSection = 947.65*0.67382*0.9702*fb;   }
  else if( channelNumber == 364188 ){  m_name = "Sherpa Wtaunu_MAXHTPTV70_140_CFilterBVeto";     m_crossSection = 946.73*0.22222*0.9702*fb;   }
  else if( channelNumber == 364189 ){  m_name = "Sherpa Wtaunu_MAXHTPTV70_140_BFilter";          m_crossSection = 943.3*0.10391*0.9702*fb;    }
  else if( channelNumber == 364190 ){  m_name = "Sherpa Wtaunu_MAXHTPTV140_280_CVetoBVeto";      m_crossSection = 339.36*0.59622*0.9702*fb;   }
  else if( channelNumber == 364191 ){  m_name = "Sherpa Wtaunu_MAXHTPTV140_280_CFilterBVeto";    m_crossSection = 339.63*0.29025*0.9702*fb;   }
  else if( channelNumber == 364192 ){  m_name = "Sherpa Wtaunu_MAXHTPTV140_280_BFilter";         m_crossSection = 339.54*0.11799*0.9702*fb;   }
  else if( channelNumber == 364193 ){  m_name = "Sherpa Wtaunu_MAXHTPTV280_500_CVetoBVeto";      m_crossSection = 72.065*0.54569*0.9702*fb;   }
  else if( channelNumber == 364194 ){  m_name = "Sherpa Wtaunu_MAXHTPTV280_500_CFilterBVeto";    m_crossSection = 71.976*0.31648*0.9702*fb;   }
  else if( channelNumber == 364195 ){  m_name = "Sherpa Wtaunu_MAXHTPTV280_500_BFilter";         m_crossSection = 72.026*0.13426*0.9702*fb;   }
  else if( channelNumber == 364196 ){  m_name = "Sherpa Wtaunu_MAXHTPTV500_1000";                m_crossSection = 15.046*1.0*0.9702*fb;       }
  else if( channelNumber == 364197 ){  m_name = "Sherpa Wtaunu_MAXHTPTV1000_E_CMS";              m_crossSection = 1.2339*1.0*0.9702*fb;       }


  /* Diboson */
  else if( channelNumber == 361063 ){  m_name = "Sherpa llll";                    m_crossSection = 12.805*0.91*fb;           }
  else if( channelNumber == 361064 ){  m_name = "Sherpa lllvSFMinus";             m_crossSection = 1.8442*0.91*fb;           }					       
  else if( channelNumber == 361065 ){  m_name = "Sherpa lllvOFMinus";             m_crossSection = 3.6254*0.91*fb;           }					       
  else if( channelNumber == 361066 ){  m_name = "Sherpa lllvSFPlus";              m_crossSection = 2.5618*0.91*fb;           }
  else if( channelNumber == 361067 ){  m_name = "Sherpa lllvOFPlus";              m_crossSection = 5.0248*0.91*fb;           }					       
  else if( channelNumber == 361068 ){  m_name = "Sherpa llvv";                    m_crossSection = 14.015*0.91*fb;           }
  else if( channelNumber == 361081 ){  m_name = "Sherpa WplvWmqq";                m_crossSection = 25.995*0.91*fb;           }
  else if( channelNumber == 361082 ){  m_name = "Sherpa WpqqWmlv";                m_crossSection = 25.974*0.91*fb;           }
  else if( channelNumber == 361083 ){  m_name = "Sherpa WlvZqq";                  m_crossSection = 12.543*0.91*fb;           }
  else if( channelNumber == 361084 ){  m_name = "Sherpa WqqZll";                  m_crossSection = 3.7583*0.91*fb;           }
  else if( channelNumber == 361085 ){  m_name = "Sherpa WqqZvv";                  m_crossSection = 7.4151*0.91*fb;           }
  else if( channelNumber == 361086 ){  m_name = "Sherpa ZqqZll";                  m_crossSection = 16.59*0.14253*0.91*fb;    }
  else if( channelNumber == 361091 ){  m_name = "Sherpa WplvWmqq_SHv21_improved"; m_crossSection = 24.893*0.91*fb;           }
  else if( channelNumber == 361092 ){  m_name = "Sherpa WpqqWmlv_SHv21_improved"; m_crossSection = 24.898*0.91*fb;           }
  else if( channelNumber == 361093 ){  m_name = "Sherpa WlvZqq_SHv21_improved";   m_crossSection = 11.5*0.91*fb;             }
  else if( channelNumber == 361094 ){  m_name = "Sherpa WqqZll_SHv21_improved";   m_crossSection = 3.4258*0.91*fb;           }
  else if( channelNumber == 361095 ){  m_name = "Sherpa WqqZvv_SHv21_improved";   m_crossSection = 6.7758*0.91*fb;           }
  else if( channelNumber == 361096 ){  m_name = "Sherpa ZqqZll_SHv21_improved";   m_crossSection = 16.432*0.14352*0.91*fb;   }
  else if( channelNumber == 361097 ){  m_name = "Sherpa ZqqZvv_SHv21_improved";   m_crossSection = 16.434*0.28245*0.91*fb;   }
  else if( channelNumber == 303014 ){  m_name = "Sherpa VV_evev_50M150";          m_crossSection = 0.82795*1.49*0.91*fb;     }
  else if( channelNumber == 303015 ){  m_name = "Sherpa VV_evev_150M500";         m_crossSection = 0.23217*1.49*0.91*fb;     }
  else if( channelNumber == 303016 ){  m_name = "Sherpa VV_evev_500M1000";        m_crossSection = 0.0093993*1.49*0.91*fb;   }
  else if( channelNumber == 303017 ){  m_name = "Sherpa VV_evev_1000M2000";       m_crossSection = 0.0011795*1.49*0.91*fb;   }
  else if( channelNumber == 303018 ){  m_name = "Sherpa VV_evev_2000M3000";       m_crossSection = 0.00012414*1.49*0.91*fb;  }
  else if( channelNumber == 303019 ){  m_name = "Sherpa VV_evev_3000M4000";       m_crossSection = 2.7399e-05*1.49*0.91*fb;  }
  else if( channelNumber == 303020 ){  m_name = "Sherpa VV_evev_4000M5000";       m_crossSection = 6.4562e-06*1.49*0.91*fb;  }
  else if( channelNumber == 303021 ){  m_name = "Sherpa VV_evev_5000M";           m_crossSection = 1.389e-06*1.49*0.91*fb;   }
  else if( channelNumber == 303046 ){  m_name = "Sherpa VV_muvmuv_50M150";        m_crossSection = 0.83003*1.49*0.91*fb;     }
  else if( channelNumber == 303047 ){  m_name = "Sherpa VV_muvmuv_150M500";       m_crossSection = 0.2335*1.49*0.91*fb;      }
  else if( channelNumber == 303048 ){  m_name = "Sherpa VV_muvmuv_500M1000";      m_crossSection = 0.0097999*1.49*0.91*fb;   }
  else if( channelNumber == 303049 ){  m_name = "Sherpa VV_muvmuv_1000M2000";     m_crossSection = 0.0011137*1.49*0.91*fb;   }
  else if( channelNumber == 303050 ){  m_name = "Sherpa VV_muvmuv_2000M3000";     m_crossSection = 0.000134*1.49*0.91*fb;    }
  else if( channelNumber == 303051 ){  m_name = "Sherpa VV_muvmuv_3000M4000";     m_crossSection = 2.74e-05*1.49*0.91*fb;    }
  else if( channelNumber == 303052 ){  m_name = "Sherpa VV_muvmuv_4000M5000";     m_crossSection = 6.4982e-06*1.49*0.91*fb;  }
  else if( channelNumber == 303053 ){  m_name = "Sherpa VV_muvmuv_5000M";         m_crossSection = 1.3982e-06*1.49*0.91*fb;  }
  else if( channelNumber == 303488 ){  m_name = "Sherpa VV_evmuv_0M150";          m_crossSection = 1.8271*1.49*0.91*fb;      }
  else if( channelNumber == 303489 ){  m_name = "Sherpa VV_evmuv_150M500";        m_crossSection = 0.45444*1.49*0.91*fb;     }
  else if( channelNumber == 303490 ){  m_name = "Sherpa VV_evmuv_500M1000";       m_crossSection = 0.015762*1.49*0.91*fb;    }
  else if( channelNumber == 303491 ){  m_name = "Sherpa VV_evmuv_1000M2000";      m_crossSection = 0.0010725*1.49*0.91*fb;   }
  else if( channelNumber == 303492 ){  m_name = "Sherpa VV_evmuv_2000M3000";      m_crossSection = 2.7134e-05*1.49*0.91*fb;  }
  else if( channelNumber == 303493 ){  m_name = "Sherpa VV_evmuv_3000M4000";      m_crossSection = 1.3573e-06*1.49*0.91*fb;  }
  else if( channelNumber == 303494 ){  m_name = "Sherpa VV_evmuv_4000M5000";      m_crossSection = 8.3335e-08*1.49*0.91*fb;  }
  else if( channelNumber == 303495 ){  m_name = "Sherpa VV_evmuv_M5000";          m_crossSection = 5.7054e-09*1.49*0.91*fb;  }
  else if( channelNumber == 304933 ){  m_name = "Sherpa VV_evtauv_0M150";         m_crossSection = 1.8280*1.49*0.91*fb;      }
  else if( channelNumber == 304934 ){  m_name = "Sherpa VV_evtauv_150M500";       m_crossSection = 0.45656*1.49*0.91*fb;     }
  else if( channelNumber == 304935 ){  m_name = "Sherpa VV_evtauv_500M1000";      m_crossSection = 0.015772*1.49*0.91*fb;    }
  else if( channelNumber == 304936 ){  m_name = "Sherpa VV_evtauv_1000M2000";     m_crossSection = 0.0010709*1.49*0.91*fb;   }
  else if( channelNumber == 304937 ){  m_name = "Sherpa VV_evtauv_2000M3000";     m_crossSection = 2.7032e-05*1.49*0.91*fb;  }
  else if( channelNumber == 304938 ){  m_name = "Sherpa VV_evtauv_3000M4000";     m_crossSection = 1.3668e-06*1.49*0.91*fb;  }
  else if( channelNumber == 304939 ){  m_name = "Sherpa VV_evtauv_4000M5000";     m_crossSection = 8.3209e-08*1.49*0.91*fb;  }
  else if( channelNumber == 304940 ){  m_name = "Sherpa VV_evtauv_M5000";         m_crossSection = 5.7052e-09*1.49*0.91*fb;  }
  else if( channelNumber == 304941 ){  m_name = "Sherpa VV_muvtauv_0M150";        m_crossSection = 1.8334*1.49*0.91*fb;      }
  else if( channelNumber == 304942 ){  m_name = "Sherpa VV_muvtauv_150M500";      m_crossSection = 0.45580*1.49*0.91*fb;     }
  else if( channelNumber == 304943 ){  m_name = "Sherpa VV_muvtauv_500M1000";     m_crossSection = 0.015743*1.49*0.91*fb;    }
  else if( channelNumber == 304944 ){  m_name = "Sherpa VV_muvtauv_1000M2000";    m_crossSection = 0.0010692*1.49*0.91*fb;   }
  else if( channelNumber == 304945 ){  m_name = "Sherpa VV_muvtauv_2000M3000";    m_crossSection = 2.7136e-05*1.49*0.91*fb;  }
  else if( channelNumber == 304946 ){  m_name = "Sherpa VV_muvtauv_3000M4000";    m_crossSection = 1.3634e-06*1.49*0.91*fb;  }
  else if( channelNumber == 304947 ){  m_name = "Sherpa VV_muvtauv_4000M5000";    m_crossSection = 8.3388e-08*1.49*0.91*fb;  }
  else if( channelNumber == 304948 ){  m_name = "Sherpa VV_muvtauv_M5000";        m_crossSection = 5.7062e-09*1.49*0.91*fb;  }
  else if( channelNumber == 304949 ){  m_name = "Sherpa VV_tauvtauv_0M150";       m_crossSection = 1.2286*1.49*0.91*fb;      }
  else if( channelNumber == 304950 ){  m_name = "Sherpa VV_tauvtauv_150M500";     m_crossSection = 0.22881*1.49*0.91*fb;     }
  else if( channelNumber == 304951 ){  m_name = "Sherpa VV_tauvtauv_500M1000";    m_crossSection = 7.9497e-03*1.49*0.91*fb;  }
  else if( channelNumber == 304952 ){  m_name = "Sherpa VV_tauvtauv_1000M2000";   m_crossSection = 5.4189e-04*1.49*0.91*fb;  }
  else if( channelNumber == 304953 ){  m_name = "Sherpa VV_tauvtauv_2000M3000";   m_crossSection = 1.3656e-05*1.49*0.91*fb;  }
  else if( channelNumber == 304954 ){  m_name = "Sherpa VV_tauvtauv_3000M4000";   m_crossSection = 6.9367e-07*1.49*0.91*fb;  }
  else if( channelNumber == 304955 ){  m_name = "Sherpa VV_tauvtauv_4000M5000";   m_crossSection = 4.3075e-08*1.49*0.91*fb;  }
  else if( channelNumber == 304956 ){  m_name = "Sherpa VV_tauvtauv_M5000";       m_crossSection = 2.9780e-09*1.49*0.91*fb;  }
  
  /* QBH */
  else if( channelNumber == 303577 ){  m_name  = "Pythia8 QBH_emu_n6 Mth03000";       m_crossSection = 0.109*fb;     }					      
  else if( channelNumber == 303578 ){  m_name  = "Pythia8 QBH_emu_n6 Mth03500";       m_crossSection = 0.0297*fb;    }					      
  else if( channelNumber == 303579 ){  m_name  = "Pythia8 QBH_emu_n6 Mth04000";       m_crossSection = 0.00839*fb;   }					      
  else if( channelNumber == 303580 ){  m_name  = "Pythia8 QBH_emu_n6 Mth04500";       m_crossSection = 0.00244*fb;   }					      
  else if( channelNumber == 303581 ){  m_name  = "Pythia8 QBH_emu_n6 Mth05000";       m_crossSection = 0.000708*fb;  }					      
  else if( channelNumber == 303582 ){  m_name  = "Pythia8 QBH_emu_n6 Mth05500";       m_crossSection = 0.000205*fb;  }					      
  else if( channelNumber == 303583 ){  m_name  = "Pythia8 QBH_emu_n6 Mth06000";       m_crossSection = 5.74e-05*fb;  }					      
  else if( channelNumber == 303584 ){  m_name  = "Pythia8 QBH_emu_n6 Mth06500";       m_crossSection = 1.59e-05*fb;  }					      
  else if( channelNumber == 303585 ){  m_name  = "Pythia8 QBH_emu_n6 Mth07000";       m_crossSection = 4.17e-06*fb;  }					      
  else if( channelNumber == 303586 ){  m_name  = "Pythia8 QBH_emu_n6 Mth07500";       m_crossSection = 1.04e-06*fb;  }					      
  else if( channelNumber == 303587 ){  m_name  = "Pythia8 QBH_emu_n6 Mth08000";       m_crossSection = 2.47e-07*fb;  }					      
  else if( channelNumber == 303610 ){  m_name  = "Pythia8 QBH_emu_n1 Mth01000";       m_crossSection = 0.543*fb;     }					      
  else if( channelNumber == 303611 ){  m_name  = "Pythia8 QBH_emu_n1 Mth01500";       m_crossSection = 0.0566*fb;    }					      
  else if( channelNumber == 303612 ){  m_name  = "Pythia8 QBH_emu_n1 Mth02000";       m_crossSection = 0.00927*fb;   }					      
  else if( channelNumber == 303613 ){  m_name  = "Pythia8 QBH_emu_n1 Mth02500";       m_crossSection = 0.00192*fb;   }					      
  else if( channelNumber == 303614 ){  m_name  = "Pythia8 QBH_emu_n1 Mth03000";       m_crossSection = 0.000451*fb;  }					      
  else if( channelNumber == 303615 ){  m_name  = "Pythia8 QBH_emu_n1 Mth03500";       m_crossSection = 0.000118*fb;  }					      
  else if( channelNumber == 303616 ){  m_name  = "Pythia8 QBH_emu_n1 Mth04000";       m_crossSection = 3.17e-05*fb;  }					      
  else if( channelNumber == 303617 ){  m_name  = "Pythia8 QBH_emu_n1 Mth04500";       m_crossSection = 8.71e-06*fb;  }					      
  else if( channelNumber == 303618 ){  m_name  = "Pythia8 QBH_emu_n1 Mth05000";       m_crossSection = 2.44e-06*fb;  }
  else if( channelNumber == 303619 ){  m_name  = "Pythia8 QBH_emu_n1 Mth05500";       m_crossSection = 6.84e-07*fb;  }
  else if( channelNumber == 303620 ){  m_name  = "Pythia8 QBH_emu_n1 Mth06000";       m_crossSection = 1.87e-07*fb;  }					      
  else if( channelNumber == 303588 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth03000";   m_crossSection = 0.108*fb;     }
  else if( channelNumber == 303589 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth03500";   m_crossSection = 0.0293*fb;    }
  else if( channelNumber == 303590 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth04000";   m_crossSection = 0.00827*fb;   }
  else if( channelNumber == 303591 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth04500";   m_crossSection = 0.00239*fb;   }
  else if( channelNumber == 303592 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth05000";   m_crossSection = 0.0007*fb;    }
  else if( channelNumber == 303593 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth05500";   m_crossSection = 0.000202*fb;  }
  else if( channelNumber == 303594 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth06000";   m_crossSection = 5.71e-05*fb;  }
  else if( channelNumber == 303595 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth06500";   m_crossSection = 1.57e-05*fb;  }
  else if( channelNumber == 303596 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth07000";   m_crossSection = 4.14e-06*fb;  }
  else if( channelNumber == 303597 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth07500";   m_crossSection = 1.04e-06*fb;  }
  else if( channelNumber == 303598 ){  m_name = "QBH+Pythia8 QBH_etau_n6 Mth08000";   m_crossSection = 2.45e-07*fb;  }
  else if( channelNumber == 303621 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth01000";   m_crossSection = 0.543*fb;     }
  else if( channelNumber == 303622 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth01500";   m_crossSection = 0.0559*fb;    }
  else if( channelNumber == 303623 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth02000";   m_crossSection = 0.00917*fb;   }
  else if( channelNumber == 303624 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth02500";   m_crossSection = 0.0019*fb;    }
  else if( channelNumber == 303625 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth03000";   m_crossSection = 0.000452*fb;  }
  else if( channelNumber == 303626 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth03500";   m_crossSection = 0.000116*fb;  }
  else if( channelNumber == 303627 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth04000";   m_crossSection = 3.1e-05*fb;   }
  else if( channelNumber == 303628 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth04500";   m_crossSection = 8.64e-06*fb;  }
  else if( channelNumber == 303629 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth05000";   m_crossSection = 2.4e-06*fb;   }
  else if( channelNumber == 303630 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth05500";   m_crossSection = 6.69e-07*fb;  }
  else if( channelNumber == 303631 ){  m_name = "QBH+Pythia8 QBH_etau_n1 Mth06000";   m_crossSection = 1.84e-07*fb;  }
  else if( channelNumber == 303599 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth03000";  m_crossSection = 0.108*fb;     }
  else if( channelNumber == 303600 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth03500";  m_crossSection = 0.0295*fb;    }
  else if( channelNumber == 303601 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth04000";  m_crossSection = 0.00835*fb;   }
  else if( channelNumber == 303602 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth04500";  m_crossSection = 0.00241*fb;   }
  else if( channelNumber == 303603 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth05000";  m_crossSection = 0.000711*fb;  }
  else if( channelNumber == 303604 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth05500";  m_crossSection = 0.000206*fb;  }
  else if( channelNumber == 303605 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth06000";  m_crossSection = 5.75e-05*fb;  }
  else if( channelNumber == 303606 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth06500";  m_crossSection = 1.57e-05*fb;  }
  else if( channelNumber == 303607 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth07000";  m_crossSection = 4.16e-06*fb;  }
  else if( channelNumber == 303608 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth07500";  m_crossSection = 1.04e-06*fb;  }
  else if( channelNumber == 303609 ){  m_name = "QBH+Pythia8 QBH_mutau_n6 Mth08000";  m_crossSection = 2.48e-07*fb;  }
  else if( channelNumber == 303632 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth01000";  m_crossSection = 0.543*fb;     }
  else if( channelNumber == 303633 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth01500";  m_crossSection = 0.0566*fb;    }
  else if( channelNumber == 303634 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth02000";  m_crossSection = 0.00926*fb;   }
  else if( channelNumber == 303635 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth02500";  m_crossSection = 0.00192*fb;   }
  else if( channelNumber == 303636 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth03000";  m_crossSection = 0.000452*fb;  }
  else if( channelNumber == 303637 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth03500";  m_crossSection = 0.000117*fb;  }
  else if( channelNumber == 303638 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth04000";  m_crossSection = 3.17e-05*fb;  }
  else if( channelNumber == 303639 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth04500";  m_crossSection = 8.77e-06*fb;  }
  else if( channelNumber == 303640 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth05000";  m_crossSection = 2.45e-06*fb;  }
  else if( channelNumber == 303641 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth05500";  m_crossSection = 6.75e-07*fb;  }
  else if( channelNumber == 303642 ){  m_name = "QBH+Pythia8 QBH_mutau_n1 Mth06000";  m_crossSection = 1.83e-07*fb;  }
  
  /* Zprime */
  else if( channelNumber == 301954 ){  m_name = "Pythia8 Zprime_emu 0500";    m_crossSection = 11.428*fb;      }
  else if( channelNumber == 301955 ){  m_name = "Pythia8 Zprime_emu 0600";    m_crossSection = 5.8245*fb;      }
  else if( channelNumber == 301956 ){  m_name = "Pythia8 Zprime_emu 0700";    m_crossSection = 3.2385*fb;      }
  else if( channelNumber == 301957 ){  m_name = "Pythia8 Zprime_emu 0800";    m_crossSection = 1.9063*fb;      }
  else if( channelNumber == 301958 ){  m_name = "Pythia8 Zprime_emu 0900";    m_crossSection = 1.1865*fb;      }
  else if( channelNumber == 301959 ){  m_name = "Pythia8 Zprime_emu 1000";    m_crossSection = 0.77863*fb;     }
  else if( channelNumber == 301960 ){  m_name = "Pythia8 Zprime_emu 1100";    m_crossSection = 0.51843*fb;     }
  else if( channelNumber == 301961 ){  m_name = "Pythia8 Zprime_emu 1200";    m_crossSection = 0.35695*fb;     }
  else if( channelNumber == 301962 ){  m_name = "Pythia8 Zprime_emu 1300";    m_crossSection = 0.25045*fb;     }
  else if( channelNumber == 301963 ){  m_name = "Pythia8 Zprime_emu 1400";    m_crossSection = 0.17783*fb;     }
  else if( channelNumber == 301964 ){  m_name = "Pythia8 Zprime_emu 1500";    m_crossSection = 0.12975*fb;     }
  else if( channelNumber == 301965 ){  m_name = "Pythia8 Zprime_emu 1600";    m_crossSection = 0.095508*fb;    }
  else if( channelNumber == 301966 ){  m_name = "Pythia8 Zprime_emu 1700";    m_crossSection = 0.071173*fb;    }
  else if( channelNumber == 301967 ){  m_name = "Pythia8 Zprime_emu 1800";    m_crossSection = 0.054205*fb;    }
  else if( channelNumber == 301968 ){  m_name = "Pythia8 Zprime_emu 1900";    m_crossSection = 0.040973*fb;    }
  else if( channelNumber == 301969 ){  m_name = "Pythia8 Zprime_emu 2000";    m_crossSection = 0.031493*fb;    }
  else if( channelNumber == 301970 ){  m_name = "Pythia8 Zprime_emu 2200";    m_crossSection = 0.018915*fb;    }
  else if( channelNumber == 301971 ){  m_name = "Pythia8 Zprime_emu 2400";    m_crossSection = 0.011765*fb;    }
  else if( channelNumber == 301972 ){  m_name = "Pythia8 Zprime_emu 2600";    m_crossSection = 0.00752*fb;     }
  else if( channelNumber == 301973 ){  m_name = "Pythia8 Zprime_emu 2800";    m_crossSection = 0.004826*fb;    }
  else if( channelNumber == 301974 ){  m_name = "Pythia8 Zprime_emu 3000";    m_crossSection = 0.0031915*fb;   }
  else if( channelNumber == 301975 ){  m_name = "Pythia8 Zprime_emu 3500";    m_crossSection = 0.0011935*fb;   }
  else if( channelNumber == 301976 ){  m_name = "Pythia8 Zprime_emu 4000";    m_crossSection = 0.00049213*fb;  }
  else if( channelNumber == 301977 ){  m_name = "Pythia8 Zprime_emu 4500";    m_crossSection = 0.00022248*fb;  }
  else if( channelNumber == 301978 ){  m_name = "Pythia8 Zprime_emu 5000";    m_crossSection = 0.00011163*fb;  }
  else if( channelNumber == 301979 ){  m_name = "Pythia8 Zprime_etau 500";    m_crossSection = 11.499*fb;      }
  else if( channelNumber == 301980 ){  m_name = "Pythia8 Zprime_etau 600";    m_crossSection = 5.7935*fb;      }
  else if( channelNumber == 301981 ){  m_name = "Pythia8 Zprime_etau 700";    m_crossSection = 3.2465*fb;      }
  else if( channelNumber == 301982 ){  m_name = "Pythia8 Zprime_etau 800";    m_crossSection = 1.9163*fb;      }
  else if( channelNumber == 301983 ){  m_name = "Pythia8 Zprime_etau 900";    m_crossSection = 1.1935*fb;      }
  else if( channelNumber == 301984 ){  m_name = "Pythia8 Zprime_etau 1000";   m_crossSection = 0.77445*fb;     }
  else if( channelNumber == 301985 ){  m_name = "Pythia8 Zprime_etau 1100";   m_crossSection = 0.52*fb;        }
  else if( channelNumber == 301986 ){  m_name = "Pythia8 Zprime_etau 1200";   m_crossSection = 0.3572*fb;      }
  else if( channelNumber == 301987 ){  m_name = "Pythia8 Zprime_etau 1300";   m_crossSection = 0.2506*fb;      }
  else if( channelNumber == 301988 ){  m_name = "Pythia8 Zprime_etau 1400";   m_crossSection = 0.17878*fb;     }
  else if( channelNumber == 301989 ){  m_name = "Pythia8 Zprime_etau 1500";   m_crossSection = 0.1297*fb;      }
  else if( channelNumber == 301990 ){  m_name = "Pythia8 Zprime_etau 1600";   m_crossSection = 0.096145*fb;    }
  else if( channelNumber == 301991 ){  m_name = "Pythia8 Zprime_etau 1700";   m_crossSection = 0.072433*fb;    }
  else if( channelNumber == 301992 ){  m_name = "Pythia8 Zprime_etau 1800";   m_crossSection = 0.053743*fb;    }
  else if( channelNumber == 301993 ){  m_name = "Pythia8 Zprime_etau 1900";   m_crossSection = 0.041235*fb;    }
  else if( channelNumber == 301994 ){  m_name = "Pythia8 Zprime_etau 2000";   m_crossSection = 0.031539*fb;    }
  else if( channelNumber == 301995 ){  m_name = "Pythia8 Zprime_etau 2200";   m_crossSection = 0.019135*fb;    }
  else if( channelNumber == 301996 ){  m_name = "Pythia8 Zprime_etau 2400";   m_crossSection = 0.011773*fb;    }
  else if( channelNumber == 301997 ){  m_name = "Pythia8 Zprime_etau 2600";   m_crossSection = 0.0074813*fb;   }
  else if( channelNumber == 301998 ){  m_name = "Pythia8 Zprime_etau 2800";   m_crossSection = 0.0048423*fb;   }
  else if( channelNumber == 301999 ){  m_name = "Pythia8 Zprime_etau 3000";   m_crossSection = 0.0031835*fb;   }
  else if( channelNumber == 302000 ){  m_name = "Pythia8 Zprime_etau 3500";   m_crossSection = 0.0011998*fb;   }
  else if( channelNumber == 302001 ){  m_name = "Pythia8 Zprime_etau 4000";   m_crossSection = 0.00048678*fb;  }
  else if( channelNumber == 302002 ){  m_name = "Pythia8 Zprime_etau 4500";   m_crossSection = 0.0002215*fb;   }
  else if( channelNumber == 302003 ){  m_name = "Pythia8 Zprime_etau 5000";   m_crossSection = 0.00011169*fb;  }
  else if( channelNumber == 302004 ){  m_name = "Pythia8 Zprime_mutau 500";   m_crossSection = 11.513*fb;      }
  else if( channelNumber == 302005 ){  m_name = "Pythia8 Zprime_mutau 600";   m_crossSection = 5.868*fb;       }
  else if( channelNumber == 302006 ){  m_name = "Pythia8 Zprime_mutau 700";   m_crossSection = 3.2393*fb;      }
  else if( channelNumber == 302007 ){  m_name = "Pythia8 Zprime_mutau 800";   m_crossSection = 1.9043*fb;      }
  else if( channelNumber == 302008 ){  m_name = "Pythia8 Zprime_mutau 900";   m_crossSection = 1.1818*fb;      }
  else if( channelNumber == 302009 ){  m_name = "Pythia8 Zprime_mutau 1000";  m_crossSection = 0.77583*fb;     }
  else if( channelNumber == 302010 ){  m_name = "Pythia8 Zprime_mutau 1100";  m_crossSection = 0.52068*fb;     }
  else if( channelNumber == 302011 ){  m_name = "Pythia8 Zprime_mutau 1200";  m_crossSection = 0.35485*fb;     }
  else if( channelNumber == 302012 ){  m_name = "Pythia8 Zprime_mutau 1300";  m_crossSection = 0.25085*fb;     }
  else if( channelNumber == 302013 ){  m_name = "Pythia8 Zprime_mutau 1400";  m_crossSection = 0.18028*fb;     }
  else if( channelNumber == 302014 ){  m_name = "Pythia8 Zprime_mutau 1500";  m_crossSection = 0.12965*fb;     }
  else if( channelNumber == 302015 ){  m_name = "Pythia8 Zprime_mutau 1600";  m_crossSection = 0.096468*fb;    }
  else if( channelNumber == 302016 ){  m_name = "Pythia8 Zprime_mutau 1700";  m_crossSection = 0.07158*fb;     }
  else if( channelNumber == 302017 ){  m_name = "Pythia8 Zprime_mutau 1800";  m_crossSection = 0.05341*fb;     }
  else if( channelNumber == 302018 ){  m_name = "Pythia8 Zprime_mutau 1900";  m_crossSection = 0.04101*fb;     }
  else if( channelNumber == 302019 ){  m_name = "Pythia8 Zprime_mutau 2000";  m_crossSection = 0.03162*fb;     }
  else if( channelNumber == 302020 ){  m_name = "Pythia8 Zprime_mutau 2200";  m_crossSection = 0.018993*fb;    }
  else if( channelNumber == 302021 ){  m_name = "Pythia8 Zprime_mutau 2400";  m_crossSection = 0.011783*fb;    }
  else if( channelNumber == 302022 ){  m_name = "Pythia8 Zprime_mutau 2600";  m_crossSection = 0.007496*fb;    }
  else if( channelNumber == 302023 ){  m_name = "Pythia8 Zprime_mutau 2800";  m_crossSection = 0.0048573*fb;   }
  else if( channelNumber == 302024 ){  m_name = "Pythia8 Zprime_mutau 3000";  m_crossSection = 0.0031528*fb;   }
  else if( channelNumber == 302025 ){  m_name = "Pythia8 Zprime_mutau 3500";  m_crossSection = 0.0011863*fb;   }
  else if( channelNumber == 302026 ){  m_name = "Pythia8 Zprime_mutau 4000";  m_crossSection = 0.0004902*fb;   }
  else if( channelNumber == 302027 ){  m_name = "Pythia8 Zprime_mutau 4500";  m_crossSection = 0.00022015*fb;  }
  else if( channelNumber == 302028 ){  m_name = "Pythia8 Zprime_mutau 5000";  m_crossSection = 0.0001115*fb;   }

  /* RPV SVT */
  else if( channelNumber == 402970 ){  m_name = "Madgraph Pythia8 SVT_emu 500";  m_crossSection = 1.8933*fb;                }
  else if( channelNumber == 402971 ){  m_name = "Madgraph Pythia8 SVT_emu 600";  m_crossSection = 0.96525*fb;               }
  else if( channelNumber == 402972 ){  m_name = "Madgraph Pythia8 SVT_emu 700";  m_crossSection = 0.53705*fb;               }
  else if( channelNumber == 402973 ){  m_name = "Madgraph Pythia8 SVT_emu 800";  m_crossSection = 0.3181*fb;                }
  else if( channelNumber == 402974 ){  m_name = "Madgraph Pythia8 SVT_emu 900";  m_crossSection = 0.19793*fb;               }
  else if( channelNumber == 402975 ){  m_name = "Madgraph Pythia8 SVT_emu 1000"; m_crossSection = 0.12785*fb;               }
  else if( channelNumber == 402976 ){  m_name = "Madgraph Pythia8 SVT_emu 1100"; m_crossSection = 0.08512*fb;               }
  else if( channelNumber == 402977 ){  m_name = "Madgraph Pythia8 SVT_emu 1200"; m_crossSection = 0.058123*fb;              }
  else if( channelNumber == 402978 ){  m_name = "Madgraph Pythia8 SVT_emu 1300"; m_crossSection = 0.040528*fb;              }
  else if( channelNumber == 402979 ){  m_name = "Madgraph Pythia8 SVT_emu 1400"; m_crossSection = 0.02875*fb;               }
  else if( channelNumber == 402980 ){  m_name = "Madgraph Pythia8 SVT_emu 1500"; m_crossSection = 0.02072*fb;               }
  else if( channelNumber == 402981 ){  m_name = "Madgraph Pythia8 SVT_emu 1600"; m_crossSection = 0.01513*fb;               }
  else if( channelNumber == 402982 ){  m_name = "Madgraph Pythia8 SVT_emu 1700"; m_crossSection = 0.011168*fb;              }
  else if( channelNumber == 402983 ){  m_name = "Madgraph Pythia8 SVT_emu 1800"; m_crossSection = 0.0083273*fb;             }
  else if( channelNumber == 402984 ){  m_name = "Madgraph Pythia8 SVT_emu 1900"; m_crossSection = 0.006263*fb;              }
  else if( channelNumber == 402985 ){  m_name = "Madgraph Pythia8 SVT_emu 2000"; m_crossSection = 0.0047488*fb;             }
  else if( channelNumber == 402986 ){  m_name = "Madgraph Pythia8 SVT_emu 2200"; m_crossSection = 0.0027918*fb;             }
  else if( channelNumber == 402987 ){  m_name = "Madgraph Pythia8 SVT_emu 2400"; m_crossSection = 0.0016763*fb;             }
  else if( channelNumber == 402988 ){  m_name = "Madgraph Pythia8 SVT_emu 2600"; m_crossSection = 0.001023*fb;              }
  else if( channelNumber == 402989 ){  m_name = "Madgraph Pythia8 SVT_emu 2800"; m_crossSection = 0.0006332*fb;             }
  else if( channelNumber == 402990 ){  m_name = "Madgraph Pythia8 SVT_emu 3000"; m_crossSection = 0.0003961*fb;             }
  else if( channelNumber == 402991 ){  m_name = "Madgraph Pythia8 SVT_emu 3500"; m_crossSection = 0.00012675*fb;            }
  else if( channelNumber == 402992 ){  m_name = "Madgraph Pythia8 SVT_emu 4000"; m_crossSection = 4.1785e-05*fb;            }
  else if( channelNumber == 402993 ){  m_name = "Madgraph Pythia8 SVT_emu 4500"; m_crossSection = 1.3953e-05*fb;            }
  else if( channelNumber == 402994 ){  m_name = "Madgraph Pythia8 SVT_emu 5000"; m_crossSection = 4.6825e-06*fb;            }
  else if( channelNumber == 402995 ){  m_name = "Madgraph Pythia8 SVT_etau 500";  m_crossSection = 1.8933*0.64069*fb;       }
  else if( channelNumber == 402996 ){  m_name = "Madgraph Pythia8 SVT_etau 600";  m_crossSection = 0.96525*0.63902*fb;      }
  else if( channelNumber == 402997 ){  m_name = "Madgraph Pythia8 SVT_etau 700";  m_crossSection = 0.53705*0.63995*fb;      }
  else if( channelNumber == 402998 ){  m_name = "Madgraph Pythia8 SVT_etau 800";  m_crossSection = 0.3181*0.63973*fb;       }
  else if( channelNumber == 402999 ){  m_name = "Madgraph Pythia8 SVT_etau 900";  m_crossSection = 0.19793*0.64564*fb;      }
  else if( channelNumber == 403000 ){  m_name = "Madgraph Pythia8 SVT_etau 1000"; m_crossSection = 0.12785*0.63976*fb;      }
  else if( channelNumber == 403001 ){  m_name = "Madgraph Pythia8 SVT_etau 1100"; m_crossSection = 0.08512*0.64102*fb;      }
  else if( channelNumber == 403002 ){  m_name = "Madgraph Pythia8 SVT_etau 1200"; m_crossSection = 0.058123*0.64208*fb;     }
  else if( channelNumber == 403003 ){  m_name = "Madgraph Pythia8 SVT_etau 1300"; m_crossSection = 0.040528*0.64552*fb;     }
  else if( channelNumber == 403004 ){  m_name = "Madgraph Pythia8 SVT_etau 1400"; m_crossSection = 0.02875*0.64844*fb;      }
  else if( channelNumber == 403005 ){  m_name = "Madgraph Pythia8 SVT_etau 1500"; m_crossSection = 0.02072*0.64235*fb;      }
  else if( channelNumber == 403006 ){  m_name = "Madgraph Pythia8 SVT_etau 1600"; m_crossSection = 0.01513*0.6508*fb;       }
  else if( channelNumber == 403007 ){  m_name = "Madgraph Pythia8 SVT_etau 1700"; m_crossSection = 0.011168*0.64721*fb;     }
  else if( channelNumber == 403008 ){  m_name = "Madgraph Pythia8 SVT_etau 1800"; m_crossSection = 0.0083273*0.64687*fb;    }
  else if( channelNumber == 403009 ){  m_name = "Madgraph Pythia8 SVT_etau 1900"; m_crossSection = 0.006263*0.64737*fb;     }
  else if( channelNumber == 403010 ){  m_name = "Madgraph Pythia8 SVT_etau 2000"; m_crossSection = 0.0047488*0.64275*fb;    }
  else if( channelNumber == 403011 ){  m_name = "Madgraph Pythia8 SVT_etau 2200"; m_crossSection = 0.0027918*0.64866*fb;    }
  else if( channelNumber == 403012 ){  m_name = "Madgraph Pythia8 SVT_etau 2400"; m_crossSection = 0.0016763*0.6497*fb;     }
  else if( channelNumber == 403013 ){  m_name = "Madgraph Pythia8 SVT_etau 2600"; m_crossSection = 0.001023*0.65313*fb;     }
  else if( channelNumber == 403014 ){  m_name = "Madgraph Pythia8 SVT_etau 2800"; m_crossSection = 0.0006332*0.64666*fb;    }
  else if( channelNumber == 403015 ){  m_name = "Madgraph Pythia8 SVT_etau 3000"; m_crossSection = 0.0003961*0.64825*fb;    }
  else if( channelNumber == 403016 ){  m_name = "Madgraph Pythia8 SVT_etau 3500"; m_crossSection = 0.00012675*0.65312*fb;   }
  else if( channelNumber == 403017 ){  m_name = "Madgraph Pythia8 SVT_etau 4000"; m_crossSection = 4.1785e-05*0.64633*fb;   }
  else if( channelNumber == 403018 ){  m_name = "Madgraph Pythia8 SVT_etau 4500"; m_crossSection = 1.3953e-05*0.64825*fb;   }
  else if( channelNumber == 403019 ){  m_name = "Madgraph Pythia8 SVT_etau 5000"; m_crossSection = 4.6825e-06*0.64653*fb;   }
  else if( channelNumber == 403020 ){  m_name = "Madgraph Pythia8 SVT_mutau 500";  m_crossSection = 1.8933*0.6418*fb;       }
  else if( channelNumber == 403021 ){  m_name = "Madgraph Pythia8 SVT_mutau 600";  m_crossSection = 0.96525*0.64565*fb;     }
  else if( channelNumber == 403022 ){  m_name = "Madgraph Pythia8 SVT_mutau 700";  m_crossSection = 0.53705*0.64548*fb;     }
  else if( channelNumber == 403023 ){  m_name = "Madgraph Pythia8 SVT_mutau 800";  m_crossSection = 0.3181*0.64639*fb;      }
  else if( channelNumber == 403024 ){  m_name = "Madgraph Pythia8 SVT_mutau 900";  m_crossSection = 0.19793*0.64326*fb;     }
  else if( channelNumber == 403025 ){  m_name = "Madgraph Pythia8 SVT_mutau 1000"; m_crossSection = 0.12785*0.64011*fb;     }
  else if( channelNumber == 403026 ){  m_name = "Madgraph Pythia8 SVT_mutau 1100"; m_crossSection = 0.08512*0.64161*fb;     }
  else if( channelNumber == 403027 ){  m_name = "Madgraph Pythia8 SVT_mutau 1200"; m_crossSection = 0.058123*0.64822*fb;    }
  else if( channelNumber == 403028 ){  m_name = "Madgraph Pythia8 SVT_mutau 1300"; m_crossSection = 0.040528*0.64598*fb;    }
  else if( channelNumber == 403029 ){  m_name = "Madgraph Pythia8 SVT_mutau 1400"; m_crossSection = 0.02875*0.64507*fb;     }
  else if( channelNumber == 403030 ){  m_name = "Madgraph Pythia8 SVT_mutau 1500"; m_crossSection = 0.02072*0.64644*fb;     }
  else if( channelNumber == 403031 ){  m_name = "Madgraph Pythia8 SVT_mutau 1600"; m_crossSection = 0.01513*0.65095*fb;     }
  else if( channelNumber == 403032 ){  m_name = "Madgraph Pythia8 SVT_mutau 1700"; m_crossSection = 0.011168*0.64612*fb;    }
  else if( channelNumber == 403033 ){  m_name = "Madgraph Pythia8 SVT_mutau 1800"; m_crossSection = 0.0083273*0.64735*fb;   }
  else if( channelNumber == 403034 ){  m_name = "Madgraph Pythia8 SVT_mutau 1900"; m_crossSection = 0.006263*0.64445*fb;    }
  else if( channelNumber == 403035 ){  m_name = "Madgraph Pythia8 SVT_mutau 2000"; m_crossSection = 0.0047488*0.65016*fb;   }
  else if( channelNumber == 403036 ){  m_name = "Madgraph Pythia8 SVT_mutau 2200"; m_crossSection = 0.0027918*0.64678*fb;   }
  else if( channelNumber == 403037 ){  m_name = "Madgraph Pythia8 SVT_mutau 2400"; m_crossSection = 0.0016763*0.65029*fb;   }
  else if( channelNumber == 403038 ){  m_name = "Madgraph Pythia8 SVT_mutau 2600"; m_crossSection = 0.001023*0.65077*fb;    }
  else if( channelNumber == 403039 ){  m_name = "Madgraph Pythia8 SVT_mutau 2800"; m_crossSection = 0.0006332*0.65197*fb;   }
  else if( channelNumber == 403040 ){  m_name = "Madgraph Pythia8 SVT_mutau 3000"; m_crossSection = 0.0003961*0.64632*fb;   }
  else if( channelNumber == 403041 ){  m_name = "Madgraph Pythia8 SVT_mutau 3500"; m_crossSection = 0.00012675*0.64935*fb;  }
  else if( channelNumber == 403042 ){  m_name = "Madgraph Pythia8 SVT_mutau 4000"; m_crossSection = 4.1785e-05*0.65039*fb;  }
  else if( channelNumber == 403043 ){  m_name = "Madgraph Pythia8 SVT_mutau 4500"; m_crossSection = 1.3953e-05*0.65265*fb;  }
  else if( channelNumber == 403044 ){  m_name = "Madgraph Pythia8 SVT_mutau 5000"; m_crossSection = 4.6825e-06*0.64622*fb;  }

  
  if( m_printInfo ){
    std::cout << "----------------------------------------------------" << std::endl;
    std::cout << "     Cross section summary:                         " << std::endl;
    std::cout << "                                                    " << std::endl;
    std::cout << "           Channel Number  = " << channelNumber       << std::endl;
    std::cout << "           process         = " << m_name              << std::endl;
    if( m_crossSection!=-1 ){
      std::cout << "           Cross section   = " << m_crossSection << " fb " << std::endl;
    }		   
    else{	   
      std::cout << "         NO Cross section -> set to -1 !"          << std::endl;
    }
  }
  
  return m_crossSection;
  
}

double MCSampleInfo :: GetCrossSection_14TeV(int channelNumber){
  
  m_crossSection = -1;

  if( m_printInfo ){
    std::cout << "----------------------------------------------------" << std::endl;
    std::cout << "     Cross section summary:                         " << std::endl;
    std::cout << "                                                    " << std::endl;
    std::cout << "           Channel Number  = " << channelNumber       << std::endl;
    std::cout << "           process         = " << m_name              << std::endl;
    if( m_crossSection!=-1 ){
      std::cout << "           Cross section   = " << m_crossSection << " fb " << std::endl;
    }
    else{
      std::cout << "         NO Cross section -> set to -1 !"          << std::endl;
    }
  }
  
  return m_crossSection;
  
}
